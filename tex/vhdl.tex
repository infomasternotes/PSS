\chapter{VHDL}
Tra i linguaggi per la descrizione di hardware, il primo è stato Verilog, linguaggio proprietario di Mentor Graphics per l'EDA (\textit{Electronic Design Automation}) di sistemi sia digitali che analogici.

Il linguaggio più diffuso è VHDL (\textit{VHSIC - Very High Speed Integrated Circuit - Hardware Description Language}): è uno standard IEEE (IEEE 1076) e serve per la progettazione di sistemi digitali, con la possibilità di modularizzare il progetto tramite la specifica di sottosistemi. Prevede anche ambienti per la simulazione dei sistemi.

È più recente, invece, il \textit{SystemC}, che è una libreria C++ per la modellazione e la simulazione di sistemi digitali, estendendo il linguaggio perché sia simile a VHDL e Verilog. Anche SystemC è uno standard IEEE (IEEE~1666).

\section{Compilazione} Il codice VHDL descrive il circuito con approccio RTL: questo viene compilato in una \textit{netlist}, che è una descrizione del circuito a livello di porte logiche che viene successivamente ottimizzata (ad esempio, applicando Quine-McCluskey).

A questo punto, il funzionamento del circuito dovrebbe essere testato con strumenti di simulazione forniti dagli IDE VHDL. Se il funzionamento è corretto, si può passare alla produzione di un sistema hardware fisico, tramite le operazioni di \textit{place and route} eseguite dagli strumenti automatici di VHDL: viene deciso il posizionamento dei componenti e i loro collegamenti.

Una volta prodotto un dispositivo hardware, esso andrà nuovamente testato per verificare che non siano insorti problemi dovuti alle caratteristiche dei componenti utilizzati. I circuiti possono essere stampati su chip custom \textit{VLSI} (\textit{Very-large-scale integration}) o su chip riprogrammabili tipo CPLD (\textit{Complex Programmable Logic Device}) o FPGA (\textit{Field Programmable Gate Array}).

\subsection{Architetture FPGA}
\begin{figure} \centering
	\caption{Connessioni dei CLB in una FPGA}
	\label{fig:clb}
	\includegraphics[width=0.8\textwidth]{img/CLB.pdf}
\end{figure}
Una FPGA è composto da una matrice di CLB (\textit{Configurable Logic Blocks}), da un DCM (\textit{Digital Clock Manager}), un blocco RAM, dei blocchi di input/output (\textit{IOB}) e da unità aritmetico-logiche (ALU, moltiplicatori, \dots ). 

Il DCM riceve in ingresso un clock esterno e produce un segnale di clock che sia un sottomultiplo (o anche un multiplo) del segnale di clock master.

I CLB (figura~\ref{fig:clb}) sono collegati tra loro attraverso la \textit{switch matrix}, che è una matrice di connessione programmabile che consente il collegamento di qualsiasi coppia di CLB. In più, i blocchi sono collegati ai propri vicini tramite un bus: i vicinati sono gruppi di quattro CLB contigui, due blocchi per la \textit{slice M} (multifunzione: logica, RAM o shift-registers) e due blocchi per la \textit{slice L} (solo logica); i due blocchi della stessa slice sono collegati in serie per quanto riguarda i riporti delle operazioni artimetico/logiche e degli shift.

\section{Tipi di dato}
I tipi di dato VHDL possono essere scalari o compositi, numerici o enumerativi, predefiniti o \textit{user defined}. Tra i tipi \textit{user defined} più comuni sono quelli nelle librerie standard IEEE ( \texttt{IEEE.std\_logic\_1164}, \texttt{IEEE.numeric\_std}, \dots ). Non tutti i tipi di dato sono sintetizzabili: alcuni sono definiti solo al fine della simulazione (ad esempio \texttt{time}). La conversione tra i diversi tipi di dato è effettuata con delle funzioni di libreria.

\paragraph{integer}
è il tipo di dato che codifica gli interi. Di default sono a 32 bit, ma è possibile specificare un \textit{range}, nel qual caso il compilatore VHDL alloca il minor numero di bit sufficienti per il range di valori indicato. \\\ \\
\begin{tabular}{ll}
	\texttt{variable n: integer} & intero a 32 bit \\
	\texttt{variable n: integer range 0 to 255} & intero a 8 bit
\end{tabular}

\paragraph{real} è il corrispettivo del tipo \texttt{float} di C. Anche in questo caso è possibile specificare un range, nel qual caso il numero di cifre significative utilizzate per indicarlo determina la precisione del numero in virgola mobile. \\\ \\
\begin{tabular}{ll}
	\texttt{variable x:} & \texttt{real} \\ & numero in virgola mobile \\
	\texttt{variable x:} & \texttt{real range 0.00 to 1.00} \\ & \texttt{float} tra 0 e 1 con precisione di 3 cifre significative 
\end{tabular}

\paragraph{Altri} tipi comuni sono \texttt{character}, \texttt{boolean} e \texttt{bit}.

\paragraph{Tipi fisici} come, ad esempio, \texttt{time} servono per esprimere grandezze fisiche (tempo, frequenza, tensione, \dots ). Il valore assegnato ad una variabile di un tipo fisico deve essere completo di unità di misura: è possibile specificare anche multipli e sottomultipli, nel qual caso possono essere compiute operazioni che coinvolgono variabili specificate con multipli diversi mantenendo la semantica fisica dell'operazione (\texttt{1 s + 1 ms = 1.001 s}).

\paragraph{Array} è usato per i tipi di dati compositi; la definizione di un tipo \texttt{array} è composta dal nome, dal \textit{range} degli indici e dal tipo di dato degli elementi: \begin{verbatim}type <name> is array (<range>) of <type>\end{verbatim}
Il range può essere definito come una successione crescente (\texttt{1 to 5} $\Rightarrow$ \texttt{[1, 2, 3, 4, 5]}), decrescente (\texttt{1 downto 5} $\Rightarrow$ \texttt{[5, 4, 3, 2, 1]}) o come gli elementi di un tipo di dato (\texttt{bit} $\Rightarrow$ \texttt{[\apex{0}, \apex{1}]}). È possibile definire tipi di dato come array multidimensionali
\begin{verbatim}type <name> is array (<range1>,<range2>,...) of <type>\end{verbatim}
I tipi di dato possono essere array \textit{constrained} (con un range limitato in definizione) o \textit{unconstrained} (con range definiti per ogni istanza).
\begin{verbatim}type word32 is array (31 downto 0) of bit -- constrained
type string is array (natural range <>) of character -- unconstrained\end{verbatim}
L'accesso agli array può essere svolto su un singolo elemento o su sottoarray\begin{verbatim}a(4) = `1';
a(3 downto 0) = b(0 to 3);\end{verbatim}

\paragraph{Subtype} permette di definire sottotipi (sottoinsiemi) di un tipo di dato come restrizione sui valori, restrizioni sugli indici (nel caso di tipi array) o come applicazioni di procedure\begin{verbatim}subtype natural is integer range 0 to integer’high
subtype word32 is bit_vector(31 downto 0)
subtype std_logic is (resolved) std_ulogic
\end{verbatim}

\paragraph{std\_logic} e \texttt{std\_ulogic} sono tipi di dato che implementano la logica a 9 valori propria dei segnali in elettronica digitale: \texttt{std\_ulogic} (\textit{unresolved logic}) rappresenta dei segnali i cui conflitti non sono risolti, mentre \texttt{std\_logic} rappresenta dei segnali risolti. Sono definiti così

\begin{multicols}{2}
\begin{verbatim} TYPE std_ulogic is
	( `U', -- Uninitialized
	`X', -- Forcing (0 or 1)
	`0', -- Forcing 0
	`1', -- Forcing 1
	`Z', -- High Impedance
	`W', -- Weak (0 or 1)
	`L', -- Weak 0 (Low)
	`H', -- Weak 1 (High)
	`-', -- Don't care
	);
\end{verbatim} \begin{flushright}{\tt \begin{tabular}{c|ccccccccc} 
		& U & X & 0 & 1 & Z & W & L & H & - \\\hline 
		U & U & U & U & U & U & U & U & U & U \\
		X & U & X & X & X & X & X & X & X & X \\
		0 & U & X & 0 & X & 0 & 0 & 0 & 0 & 0 \\
		1 & U & X & X & 1 & 1 & 1 & 1 & 1 & X \\
		Z & U & X & 0 & 1 & Z & W & L & H & X \\
		W & U & X & 0 & 1 & W & W & W & W & X \\
		L & U & X & 0 & 1 & L & W & L & W & X \\
		H & U & X & 0 & 1 & H & W & W & H & X \\
		- & U & X & X & X & X & X & X & X & X  
	\end{tabular}}\end{flushright}
\end{multicols}\noindent 
Gli \texttt{std\_ulogic} possono essere usati per risolvere i conflitti tra segnali: il risultato del conflitto tra due \texttt{std\_ulogic} è uno \texttt{std\_logic}. Le librerie \texttt{ieee.std\_logic\_arith} e \texttt{ieee.std\_numeric} permettono di utilizzare gli array di \texttt{std\_logic} (\texttt{std\_logic\_vectors}) nelle operazioni aritmetiche: questo consente di bypassare la conversione da \texttt{std\_logic} (il tipo della porta di ingresso del sistema) a tipo numerico (per svolgere le operazioni) e di nuovo a \texttt{std\_logic} (il tipo della porta di uscita).

Tra i tipi più usati abbiamo \texttt{signed} e \texttt{unsigned} (interi con e senza segno) e \texttt{sfixed} e \texttt{ufixed} (decimali in virgola fissa con e senza segno).

\begin{verbatim}
type unsigned is array (natural range <>) of std_logic
type signed is array (natural range <>) of std_logic
type ufixed is array (integer range <>) of std_logic
type sfixed is array (integer range <>) of std_logic
\end{verbatim}
Sia nel caso degli interi che nel caso dei numeri in virgola fissa, l'indice dell'array è la potenza di due corrispondente alla posizione: per cui le cifre decimali hanno indice negativo, mentre \texttt{signed} e \texttt{unsigned} accettano solo indici positivi. Analogamente, sono definiti dei tipi \texttt{float} per i numeri in virgola mobile
\begin{verbatim}
type float is array (integer range <>) of std_logic
subtype float32 is float (8 downto -23) -- single precision
subtype float64 is float (11 downto -52) -- double precision
subtype float128 is float (15 downto -112) -- quadruple precision
\end{verbatim}
Il MSB è il segno, i restanti bit di indice naturale sono la mantissa (privata del primo \texttt{\apex{1}}) e i bit di indice negativo sono l'esponente.

\paragraph{record} sono l'analogo delle \texttt{sruct} C e implementano tipi composti da elementi di tipo diverso. L'accesso al campo di un record si effettua con la notazione puntata.

\section{Oggetti}
In VHDL gli \quoted{oggetti} possono essere costanti, variabili, file o segnali.

\subsection{Costanti}
\begin{verbatim}constant <name> : <type> [:= <value>]\end{verbatim}
Le costanti sono istanze di un tipo di dato il cui valore, una volta assegnato, non cambia. In alcune librerie sono presenti costanti dichiarate, ma non definite, a cui deve essere assegnato un valore dall'utente per utilizzare la libreria. Dove un valore sia una costante è molto importante definirlo come tale in VHDL: infatti, la sua definizione come variabile sarebbe molto meno efficiente, in quanto implicherebbe la messa a disposizione dei registri necessari.

\subsection{Variabili}
\begin{verbatim}variable <name> : <type> [:= <value>]\end{verbatim}
Le variabili sono istanze di un tipo di dato il cui valore cambia. Le variabili sono utilizzate all'interno dei costrutti sequenziali di VHDL (processi, funzioni/procedure).

\subsection{Segnali}
\begin{verbatim}signal <name> : <type> [<= <value>]\end{verbatim}
I segnali in VHDL sono strutture per la rappresentazione di segnali fisici come funzione del tempo: l'assegnamento di un valore ad un segnale è legato ad un istante di tempo e il segnale manterrà il valore assegnato fino all'assegnamento successivo. L'intervallo di tempo a cui fare l'assegnamento può essere specificato, altrimenti si considera l'istante corrente.
\begin{verbatim}
signal x : bit ;
x <= `0';           -- istante corrente
x <= `0' after 1 s; -- 1 secondo dopo
x <= `1' after 4 s; -- 4 secondi dopo
\end{verbatim}
L'assegnamento può essere effettuato con ritardo di trasporto e con ritardo di inerzia: il ritardo di trasporto è il tempo dopo il quale il segnale viene propagato da un terminale ad un altro, mentre il ritardo di inerzia è il tempo durante il quale il segnale deve rimanere stabile (costante) perché il circuito reagisca al cambiamento.
\begin{verbatim}
x <= a or b after 11 ns; -- ritardo di inerzia
x <= transport(a or b) after 14 ns; -- ritardo di trasporto
\end{verbatim}
La dichiarazione di segnali può avvenire come \begin{itemize}
	\item Segnali di interfaccia, nello header delle entità \\ \texttt{port( <signal>: <mode> <type> )}
	\item Segnali interni, nella parte dichiarativa delle architetture \\ \texttt{signal <signal>: <type> [:=init\_value]}
\end{itemize}
Si evidenzia che, nel contesto di un codice concorrente, una grandezza che varia il proprio valore nel corso dell'esecuzione del componente andrà modellata come un segnale e non come una variabile: queste, infatti, possono essere usate solo nei costrutti sequenziali. Nei costrutti sequenziali l'assegnamento dei valori a variabili è sequenziale, mentre l'assegnamento dei valori ai segnali è concorrente, alla fine del ciclo di esecuzione.

\subsection{File}
I file sono definiti di un tipo che è un tipo di file: alla definizione dell'oggetto file è possibile specificare la modalità di apertura (\texttt{read\_mode}, \texttt{write\_mode} o \texttt{append\_mode})
\begin{verbatim}type <type_name> is file of <data_type>
file <name> : <type_name> [ [open <open_mode>] is <exp> ] \end{verbatim}	

\section{Struttura del codice}
Il codice VHDL si divide in tre sezioni\begin{itemize}
	\item \textit{Library declarations} \\ in cui vengono dichiarate le librerie utilizzate nel progetto, come in un linguaggio di programmazione
	\item \textit{Entity} \\ definisce l'interfaccia del modulo, ovvero il suo progetto \quoted{visto da fuori}
	\item \textit{Architecture} \\ definisce la struttura e il funzionamento del componente, ovvero il suo progetto \quoted{visto da dentro}
\end{itemize}
Un'architettura può includere un altro componente, implementando il paradigma della modularità.
Inoltre, è possibile specificare più architetture per la stessa entità: ad esempio, si può specificare un'architettura basata su approccio \textit{dataflow}, specificando in modo stretto il progetto hardware, ed un'altra che ne specifica il \textit{behaviour}, che sarà, quindi, più funzionale.

\subsection{Entity}
\begin{verbatim}
entity <name> is
    [ generic ( <generic_list> ); ]
    [ port ( <port_list> ); ]
    { <entity_declarative_items> }
[ begin
    <entity_statement_part> ]
end [entity] [<identifier>] ;
\end{verbatim}
Lo header dell'entity è dato dalle liste \textit{generic} e \textit{port}. Generic prevede le definizioni di costanti e parametri di progetto, mentre port prevede la lista degli input e output del componente.\\\ \\
es. \texttt{generic( constant max\_clock : frequency := 30 MHz );}\\\ \\
Port prevede la definizione dei segnali di input e output.\begin{verbatim}
port( [signal] <name> : [in | out] <type> );
\end{verbatim}

\subsection{Architecture}
\begin{verbatim}
architecture <name> of <entity> is
    { <architecture_declarative_part> }
begin
    { <architecture_concurrent_part> }
end [ <simple_name> ]
\end{verbatim}
L'architettura può contenere una parte dichiarativa in cui è possibile dichiarare sottoprogrammi, tipi (e sottotipi), costanti, segnali, alias, componenti, specificare la configurazione e utilizzare la \texttt{use} clause.

La parte concorrente, invece, contiene un insieme di statement che vengono eseguiti in parallelo, poiché costituiscono la definizione di un circuito, in cui non ha senso parlare di sequenzialità.

\section{Operatori}
Gli operatori disponibili in VHDL sono gli operatori:\begin{itemize}
	\item logici\\ not, and, or, xor, nand, nor, xnor
	\item aritmetici\\ +, -, *, /, **, mod, rem, abs
	\item confronto\\ =, /=, \textless, \textgreater, \textless=, \textgreater=
	\item concatenazione\\ \&
	\item shift (logico/aritmetico/rotazione)\\ sll, srl, sla, sra, rol, ror
\end{itemize}

\section{Attributi}
In VHDL gli oggetti e i tipi hanno degli attributi, che possono essere, \textit{data atributes} (tipi e array) o \textit{signal attributes}.
È possibile definire dei nuovi attributi rispetto a quelli predefiniti 
\begin{verbatim}
<name>'<attribute>
attribute <name>: <type>;
attribute <name> of <target>: <class> is <value>;
\end{verbatim}
Gli attributi di tipo più comuni sono
\begin{multicols}{3}\begin{verbatim}
T'LOW
T'HIGH
T'LEFT
T'RIGHT
T'PREC(X)
T'SUCC(X)
T'LEFTOF(X)
T'RIGHTOF(X)
T'POS(X)
T'VAL(N)
\end{verbatim}\end{multicols}\noindent 
La differenza fra \texttt{left} e \texttt{low} (e analogamente, tra \texttt{right} e \texttt{high}, etc\dots ) è che \texttt{low} restituisce l'elemento inferiore secondo l'ordinamento del tipo (\textless), mentre \texttt{left} restituisce il primo valore in ordine di definizione.
Sono analoghi gli attributi degli array (per gli array monodimensionali non deve essere specificata la dimensione \texttt{N}): non restituiscono i valori degli elementi, ma degli indici.
\begin{multicols}{2}\begin{verbatim}
A'LOW(N)
A'HIGH(N)
A'LEFT(N)
A'RIGHT(N) 
A'RANGE(N)
A'REVERSE_RANGE(N)
A'LENGTH(N)
\end{verbatim}\end{multicols}\noindent 
Sono diversi, invece, gli attributi dei segnali: essi ci permettono di accedere a informazioni riguardo la stabilità del segnale.
\begin{multicols}{4}\begin{verbatim}
	S'DELAYED[(T)]
	S'LAST_VALUE
	S'EVENT
	S'ACTIVE
	S'STABLE[(T)]
	S'QUIET[(T)]
	S'LAST_EVENT
	S'LAST_ACTIVE
\end{verbatim}\end{multicols}\noindent
L'attributo \texttt{S'DELAYED(T)} restituisce il valore del segnale un intervallo di tempo \texttt{T} prima dell'istante attuale: in un istante di tempo in cui avviene una variazione, abbiamo che \texttt{S'DELAYED(0 ns)} $\neq$ \texttt{S} e \texttt{S'DELAYED(0 ns)} = \texttt{S'LAST\_VALUE}, che è il valore del segnale prima dell'ultima variazione. 

Negli istanti di tempo in cui il valore del segnale cambia (e solo in quegli istanti), abbiamo che \texttt{S'EVENT} = \texttt{TRUE}. L'attributo \texttt{S'STABLE(T)} è \texttt{TRUE} se e solo se non ci sono stati eventi nell'intervallo di tempo di larghezza \texttt{T} precedente all'istante corrente ($S'STABLE(T) \Leftrightarrow S'DELAYED(t) = S\ \forall t \in [0, T]$): quindi vale sempre \texttt{S'STABLE /= S'EVENT} (omettendo \texttt{T} si implica \texttt{T := 0}). L'attributo \texttt{S'LAST\_EVENT} restituisce l'istante di tempo minimo \texttt{T} tale che \texttt{S'STABLE(T) = FALSE}, se il segnale è stabile dall'origine dei tempi, restituisce \texttt{TIME'HIGH}.

Negli istanti di tempo in cui il segnale subisce un assegnamento anche senza cambiare (e solo in quegli istanti), abbiamo che \texttt{S'ACTIVE} = \texttt{TRUE}. L'attributo \texttt{S'QUIET(T)} è \texttt{TRUE} se e solo se il segnale non è stato mai attivo nell'intervallo di tempo di larghezza \texttt{T} precedente all'istante corrente: quindi vale sempre \texttt{S'QUIET /= S'ACTIVE}. L'attributo \texttt{S'LAST\_ACTIVE} restituisce l'istante di tempo minimo \texttt{T} tale che \texttt{S'QUIET(T) = FALSE}, se il segnale è inattivo dall'origine dei tempi, restituisce \texttt{TIME'HIGH}.

\section{Costrutti concorrenti}

\subsection{Assegnamenti a segnali}
L'assegnamento a un segnale si effettua con l'operatore \texttt{\textless=}: a un segnale si può assegnare uno scalare, cambiando il suo valore da un istante (presente o futuro) in poi, oppure un altro segnale (eventualmente specificando i ritardi inerziale e di trasporto).

\subsubsection{WHEN/ELSE}\begin{verbatim}
S <= <value1> WHEN <condition> ELSE
     <value2>;
\end{verbatim}Consente di effettuare assegnamenti condizionali: se la condizione specificata dopo \texttt{WHEN} è vera, si effettua il primo assegnamento, altrimenti il secondo (è possibile annidare).
\begin{verbatim}
-- Multiplexer
S <= a WHEN s="00" ELSE
     b WHEN s="01" ELSE
     c WHEN s="10" ELSE
     d WHEN s="11" ELSE
    `Z';
\end{verbatim}

\subsubsection{WITH/SELECT/WHEN}
Consente di fare assegnamenti selettivi in base al valore dell'identificatore specificato dopo \texttt{WITH}: i valori con cui viene confrontato possono essere valori singoli, range (\texttt{<idvalueStart> to <idvalueEnd>}), insiemi (\texttt{<idvalueA> | <idvalueB> | ... }) o \texttt{OTHERS} per specificare tutti i casi rimanenti.\begin{verbatim}
WITH <identifier> SELECT
S <= <value1> WHEN <idvalue1> ELSE,
     ...
     <valueN> WHEN OTHERS;
\end{verbatim}
 I casi non devono intersecarsi e devono coprire l'insieme dei valori assumibili dall'identificatore. Se in un caso non vogliamo effettuare un assegnamento possiamo specificare \texttt{UNAFFECTED}.

\subsection{GENERATE}
\begin{verbatim}
	[<label>] FOR <identifier> IN <range> GENERATE
	    {<concurrent statements>}
	END GENERATE
\end{verbatim}
\begin{verbatim}
	[<label>] IF <condition> GENERATE
	    {<concurrent statements>}
	END GENERATE
\end{verbatim}Il costrutto \textit{generate} non deve essere confuso con un ciclo for o un controllo di flusso: esso genera le istruzioni concorrenti associate ad ogni elemento del range (o sceglie se generare o meno il blocco di istruzioni) in modo statico (al più parametrico, ma non variabile). Inoltre, le istruzioni generate sono da considerarsi concorrenti, come il resto del codice architetturale.

\subsection{BLOCK}
Il costrutto \textit{block} definisce un sottocomponente.
\begin{verbatim}
	<label>: BLOCK [<guard_expression>]
	    [<declarative_part>]
	BEGIN
	    {<concurrent statements>}
	END BLOCK <label>;
\end{verbatim}Inoltre, è possibile specificare una \textit{guard expression} rendendo il blocco un \textit{guarded block}: gli assegnamenti in cui viene posta l'etichetta \texttt{GUARDED} vengono valutati solo se la \textit{guard expression} è vera.
\begin{verbatim}
fflop: BLOCK (C'EVENT and C=`1')
BEGIN
	Q <= GUARDED `0' WHEN R ELSE D
END BLOCK fflop;
\end{verbatim}

\section{Costrutti sequenziali}
Esistono costrutti in VHDL che permettono l'utilizzo di codice sequenziale, questi sono i processi e i sottoprogrammi (funzioni e procedure): i sottoprogrammi si possono trovare nella parte dichiarativa di una \texttt{ENTITY}, in una architettura o in un pacchetto.

\paragraph{Shared variables}
sono dichiarate esternamente ai processi e possono essere accedute da più processi diversi: VHDL non implementa alcuna politica di gestione degli accessi concorrenti, per cui sono da usare con cautela.

\paragraph{Controllo di flusso}
I costrutti per il controllo del flusso nel codice concorrente ricalcano quelli presenti nei più comuni linguaggi di programmazione.
\begin{multicols}{2}
\begin{verbatim}
IF <condition> THEN 
    <statements>
[ ELSIF <condition> THEN 
    <statements> ]
[ ELSE
    <statements> ]
END IF
\end{verbatim}
\columnbreak
\begin{verbatim}
CASE <var/sig> IS
    WHEN <value> => <statements>;
    WHEN <value> => NULL;
    WHEN OTHERS => <statements>
END CASE
\end{verbatim}
\end{multicols}
\begin{verbatim}
LOOP
    <statements>;
    [NEXT [WHEN <condition>]] -- continue
    [EXIT [WHEN <condition>]] -- break
END LOOP
\end{verbatim}
\begin{multicols}{2}
	\begin{verbatim}
FOR <var> IN <range> LOOP
	<statements>;
END LOOP
	\end{verbatim}
	\columnbreak
	\begin{verbatim}
WHILE <condition> LOOP
<statements>;
END LOOP
	\end{verbatim}
\end{multicols}

\subsection{PROCESS}Un processo è una sezione di codice sequenziale che viene eseguito ad ogni istante, come se fosse una delle altre istruzioni concorrenti: la sua durata in simulazione è nulla \begin{verbatim}
[<label>:] PROCESS [ (<sensitivity_list>) ]
    [<declarative_part>]
BEGIN
    [<statements>]
END PROCESS [ <label> ];
\end{verbatim}
La \textit{sensitivity list} di un processo è la lista dei segnali il cui cambiamento provoca l'attivazione del processo: in ogni istante di esecuzione del componente il processo viene valutato solo se un segnale tra quelli nella sensitivity list ha un evento.
Un altro modo per arrestare l'esecuzione di un processo è lo statement \texttt{WAIT}\begin{itemize}
	\item \texttt{WAIT FOR <time>}\\ Arresta il processo per la durata specificata
	\item \texttt{WAIT UNTIL <condition>}\\ Arresta il processo fintanto che la condizione rimane falsa
	\item \texttt{WAIT ON <signal>}\\ Arresta il processo fino al successivo evento del segnale
	\item \texttt{WAIT [ON <signal>] UNTIL <condition> [FOR <time>] }\\ \textit{Complex wait}, include tutte le possibilità di wait
\end{itemize}
L'utilizzo dello statement \texttt{WAIT} e la specificazione di una \textit{sensitivity list} sono mutualmente esclusivi in un processo.

\subsection{FUNCTION}
\begin{verbatim}
[ PURE | IMPURE ] FUNCTION <label> (<params>)
    RETURN <type> IS
        <declarative_part>
    BEGIN
        <statements>
    RETURN <something>
END <label>;
\end{verbatim}
Una funzione \texttt{PURE} VHDL corrisponde al concetto matematico di funzione, ovvero di una mappa tale che \begin{itemize}
	\item Il valore di ritorno sia uguale per input uguali 
	\item Non abbia stato (memoria)
	\item Modifichi solo lo scope locale
\end{itemize}
Se non soddisfa queste condizioni necessarie, la funzione è \texttt{IMPURE}. I parametri di input possono essere costanti o segnali. VHDL supporta l'\textit{overloading} e permette di ridefinire gli operatori.

\subsection{PROCEDURE}\begin{verbatim}
PROCEDURE <label> ( [ IN    <inputparams>  |        -- sola lettura
                      OUT   <outputparams> |        -- sola scrittura
                      INOUT <ioparams> ] )   		-- lettura/scrittura
    IS
        <declarative_part>
    BEGIN 
        <statements>
END <label>;
\end{verbatim}
Una procedura può avere un numero a piacere di parametri, sia in entrata che in uscita. La chiamata a procedura costituisce uno statement autonomo (mentre le funzioni possono far parte di espressioni complesse).

\section{Asserzioni}
\begin{verbatim}
TYPE severity_level IS ( note , warning , error , failure );
ASSERT <expression> [ REPORT <msg> ] [ SEVERITY <level> ]
\end{verbatim}
Le asserzioni non sono sintetizzabili, ma sono utilissime in simulazione per il debugging. Quando non è soddisfatta genera un \texttt{REPORT}: i report di livello \texttt{note} e \texttt{warning} non fermano la simulazione, \texttt{error} e \texttt{failure} sì.

\section{Librerie}
Il codice VHDL può importare da librerie: esse sono composte da \textit{package}, che contengono componenti come funzioni e procedure.
\begin{verbatim}
LIBRARY <name>;
USE <library>.<package>.<element>; -- .all per tutti gli elementi
\end{verbatim}
Le librerie standard sono \texttt{std}, \texttt{ieee} e \texttt{work} (il pacchetto utente predefinito): le librerie \texttt{std} e \texttt{work} non sono da dichiarare, sono implicitamente importate. Creando un pacchetto, automaticamente sarà nella libreria \texttt{work}
\begin{verbatim}
PACKAGE <label> IS
    <declarative_part>
END <label>

[ PACKAGE BODY <label> IS
    <package_components>
END <label>; ]
\end{verbatim}

\section{Istanziazione di sottocomponenti}
Il metodo più efficace per progettare in VHDL è di specificare strutturalmente il progetto di alto livello (tramite una divisione in blocchi, la definizione dei collegamenti tra i blocchi e dei segnali di controllo), e definire funzionalmente i blocchi di basso livello. Le tecniche di istanziazione permettono di implementare questo paradigma.

\subsection{Istanziazione diretta}
La tecnica dell'istanziazione diretta consiste nell'istanziare all'interno di un'architettura un modulo già definito come un'altra entità. 
\begin{verbatim}
<label>: ENTITY <package>.<entity>[(architecture)] 
PORT MAP(<port_signal_mappings>);
\end{verbatim}
Per istanziare un componente già progettato è necessario mappare i segnali di interfaccia: \texttt{<subcomponent\_signal> => <supercomponent\_signal>}.

È possibile specificare il nome del segnale nel supercomponente e nel sottocomponente (\textit{named port association}) oppure specificare solo i nomi dei segnali nel supercomponente: in questo caso vengono associati ai segnali del sottocomponente a seconda dell'ordine di dichiarazione nell'entity (\textit{positional port association}). Il mapping dei segnali può essere effettuato in modalità\begin{itemize}
	\item \textit{Simple} (mappatura dei segnali singoli): ogni segnale viene mappato su un altro segnale
	\item \textit{Complex} (mappatura di array di segnali): dati due array di segnali di uguale dimensione, ogni segnale elemento di un array viene mappato sul segnale elemento dell'altro array che gli corrisponde nell'ordinamento rispettivo degli array
	\item \textit{Open} (segnale non mappato): la keyword \texttt{open} indica che sul segnale del sottocomponente non viene mappato nessun segnale (terminale scollegato)
\end{itemize}

\paragraph{Block}\begin{verbatim}
[<label> :] block 
    port(<port_listing>)
    [generic(<generic_listing>)]
    port map(<port_mapping>)
    [<declarations>]
begin 
    [<operations>]
end block [<label>]
\end{verbatim}
Il costrutto \texttt{block} permette di istanziare direttamente un sottocomponente definito al momento. Questo permette di organizzare strutturalmente il codice anche nei casi in cui il sottocomponente viene istanziato una tantum.

\subsection{Istanziazione indiretta}\begin{multicols}{2}\begin{verbatim}
-- declaration
component <comp_name> 
    [<generic_listing>]
    [<port_listing>]
end component;
-- instantiation
<label>: [component] <comp_name> 
    [<generic_mapping>]
    [<port_mapping>];
\end{verbatim}\end{multicols}
L'istanziazione indiretta prevede che le interfacce dei \texttt{component} utilizzati siano dichiarate nella parte dichiarativa dell'\texttt{architecture} del supercomponente. Dato che l'insieme di queste dichiarazioni può risultare pesante in progetti con molti componenti è possibile scriverle in un file separato, che poi verrà importato.
\paragraph{Configurazione} 
Il vantaggio dell'istanziazione indiretta rispetto all'istanziazione diretta è la possibilità di specificare la configurazione dei componenti: questo è un file che indica per ogni architettura dell'entità, quali architetture dei componenti utilizzare nell'istanziazione.\begin{verbatim}
configuration <config_name> of <entity_name> is
    [<declarations>]
    { for <architecture_name>
        { for <component_list> | others | all : <component_name>
            [ use 
                entity <component_entity>[(<component_arch>)] 
            |   configuration <component_configuration>
            |   open    ;]
            [<generic_mapping>;]
            [<port_mapping>;]
        end for; }
    end for; }
end [<config_name>] ;
\end{verbatim}

\section{Test Bench}\begin{verbatim}
entity testBench is
end entity testBench;
architecture <...> of testBench is
<i/o signals declarations>
begin 
<UUT instantiation>
<stimuli generation>
end
\end{verbatim}
Il testing di un circuito in VHDL avviene attraverso la progettazione di un \textit{test bench}: questo è un circuito VHDL (utilizzabile solo in simulazione) che genera dei segnali in ingresso alla UUT (\textit{unit under test}) e ne rileva i segnali in uscita. I segnali in ingresso generati sono detti \textit{stimuli}. Il circuito test bench non ha segnali di interfaccia.

La definizione degli stimuli può avvenire in modalità concorrente o sequenziale. La definizione concorrente degli stimuli avviene tramite l'assegnamento con \texttt{after} dei valori del segnale nel tempo, la definizione sequenziale avviene all'interno di un processo che assegna il valore ai segnali e poi fa una \texttt{wait} fino all'evento successivo.
La definizione concorrente è utile quando è già noto a priori lo sviluppo di ogni stimulus nel tempo: quando, invece, gli stimuli sono determinati da proprietà emergenti o dalle relazioni con altri stimuli, allora può essere più comodo un approccio sequenziale.

\paragraph{Assert} è uno strumento molto utile per il debugging, pertanto nel contesto della definizione di un test bench vengono spesso inserite delle asserzioni.
\chapter{Sensori e Attuatori}
Un trasduttore è un sistema in grado di convertire una grandezza fisica in un'altra: ai fini della misurazione digitale, un trasduttore trasforma una grandezza da misurare in una grandezza elettrica (tensione, intensità, \dots ).
Il sensore è l'elemento sensibile alla grandezza fisica da misurare, mentre il trasduttore è l'intero dispositivo.

\section{Caratteristiche}
Le proprietà di un sensore vengono osservate sotto tre punti di vista differenti: le caratteristiche statiche descrivono il funzionamento del segnale per input costanti (\,$y=f(x)$\,), le caratteristiche dinamiche descrivono il funzionamento del sensore con input variabili nel tempo (\,$y_t=f_t(x_t)$\,) e la dipendenza da altre grandezze esprime come fenomeni diversi da quello che si intende misurare influenzino il funzionamento del sensore (\,$y=f(x^{(0)}|x^{(1)},x^{(2)},\dots)$\,)\,.

\subsection{Parametri statici}
I parametri della caratteristica statica di un trasduttore sono \begin{itemize}
	\item Range dinamico: estremi dei valori di input e di output del trasduttore 
	\item Linearità: aderenza della funzione di trasduzione ad una retta. Tanto più lineare è un trasduttore tanto più $y=f(x)\rightarrow Gx+b$
	\item Sensibilità: capacità di rilevare piccole variazioni nell'input. Se definiamo il guadagno del sensore $G(x)=\frac{df}{dx}(x)$\,, esso è un indicatore della sensibilità
	\item Risoluzione: minima variazione dell'input rilevabile dal sistema. Ad esempio, se il sistema comprende un ADC, è l'intervallo $\Delta$ di quantizzazione
	\item Accuratezza: vicinanza della misura al valore reale del segnale
	\item Precisione: coerenza di misure ripetute della stessa grandezza
\end{itemize}

\subsection{Parametri dinamici}
\begin{figure} \centering
	\caption{Effetto della caratteristica dinamica di un sensore: in blu è mostrato un segnale di ingresso, in rosso la risposta del sensore}
	\label{fig:dynamics}
	\ \newline 
	\includegraphics[width=\textwidth,trim={2in 0.4in 2in 0.4in},clip]{pyplots/dynamics.pdf}
\end{figure}
I parametri della caratteristica dinamica di un trasduttore sono \begin{itemize}
	\item Tempo di salita (\textit{rise time}): tempo di risposta ad una variazione istantanea a gradino, definito come l'intervallo di tempo che intercorre dal raggiungimento del $10\%$ al raggiungimento del $90\%$ del gradino 
	\item \textit{Slew rate}: la massima velocità con cui può variare l'uscita del sensore (\,$\max|\nicefrac{dy(t)}{dx(t)}|$\,)
	\item Costante di tempo: tempo di risposta esponenziale al gradino 
	\item Risposta all'impulso: uscita relativa ad un input impulsivo (\,$h(t)=f(\delta(t))$\,)\begin{itemize}
		\item La risposta ad un input arbitrario è data dal prodotto di convoluzione per la risposta all'impulso $y(t)=h(t)\ast x(t)$
		\item La sua trasformata è la funzione di trasferimento ed esprime la risposta in frequenza $H(\nu)=\nicefrac{Y(\nu)}{X(\nu)}$: il modulo $|H(\nu)|$ indica il guadagno e la fase $\angle H(\nu)$ il ritardo (lo sfasamento) 
	\end{itemize}
\end{itemize}

\subsection{Tassonomia}
I trasduttori vengono classificati a seconda della grandezza elettrica in uscita, in base al fenomeno fisico sfruttato e alla dimensionalità del segnale acquisito.
\begin{itemize}
	\item Grandezza in uscita
	\begin{itemize}
		\item Tensione
		\item Corrente
		\item Resistenza $\rightarrow$ trasduttori resistivi
		\item Capacità $\rightarrow$ trasduttori capacitativi
		\item Induttanza $\rightarrow$ trasduttori induttivi
	\end{itemize}
	\item Fenomeno fisico
	\begin{itemize}
		\item Piezoelettricità $\rightarrow$ trasduttori piezoelettrici
		\item Termoelettricità $\rightarrow$ trasduttori termoelettrici
		\item Effetto Fotoelettrico $\rightarrow$ trasduttori fotovoltaici
	\end{itemize}
	\item Dimensionalità
	\begin{itemize}
		\item Acquisizione di vettori di segnali \begin{itemize}
			\item[1D] Es. segnali semplici $f(t)$ (temperatura, pressione, \dots )
			\item[2D] Es. velocità sul piano $\langle f_x(t), f_y(t) \rangle$
			\item[3D] Es. accelerazione nello spazio $\langle f_x(t), f_y(t), f_z(t) \rangle$
			\item[6D] Es. campo elettromagnetico $\langle E_{xyz}(t), H_{xyz}(t) \rangle$
		\end{itemize}
		\item Acquisizione di segnali multidimensionali \begin{itemize}
			\item[1D] Es. suono $f(t)$ 
			\item[2D] Es. immagini $f(x,y)$
			\item[3D] Es. video $f(t,x,y)$
		\end{itemize}
	\end{itemize}
\end{itemize}

\subsection{Condizionamento del Segnale}
Un trasduttore produce in uscita una grandezza elettrica: tale segnale elettrico deve essere condizionato perché sia adatto ad entrare nell'ADC. Le operazioni di condizionamento più comuni sono:\begin{itemize}
	\item Amplificazione \begin{itemize}
		\item Conversione della grandezza elettrica prodotta nella grandezza di ingresso dell'ADC (tipicamente tensione)
		\item Adattamento del range dinamico del segnale al range di misura dell'ADC
	\end{itemize}
	\item Eccitazione \begin{itemize}
		\item Alimentazione del sensore con una tensione (o corrente) nota 
		\item Alimentazione a corrente alternata (sensori capacitativi)
	\end{itemize}
	\item Linearizzazione
	\item Filtraggio \begin{itemize}
		\item Riduzione rumore
		\item Anti-aliasing
	\end{itemize}
	\item Isolamento
\end{itemize}

\section{Sensori di Temperatura}
\subsection{Termocoppia}
\begin{figure} \centering
	\caption{Circuito di una termocoppia: i riquadri colorati di rosso e blu indicano rispettivamente le zone alla temperatura da misurare e di riferimento. I tre colori per i conduttori indicano tre materiali differenti}
	\label{fig:termocoppia}
	\ \newline 
	\includegraphics[width=0.6\textwidth]{img/thermocouple.pdf}
\end{figure}
Le termocoppie sono sensori di temperatura che sfruttano l'effetto Seebeck (effetto termoelettrico): la giunzione tra due metalli diversi genera una differenza di potenziale funzione della temperatura al punto di giunzione. La trasduzione è, quindi, direttamente da temperatura a tensione.

La giunzione tra i due metalli principali (in figura~\ref{fig:termocoppia}, i metalli rosso e nero) avviene alla temperatura $T_x$ da misurare. Le giunzioni con il conduttore (blu in figura) avvengono ad una temperatura controllata $T_0$. Chiamati $A$ e $B$ i due metalli della termocoppia e $C$ il conduttore, la differenza di potenziale ai capi del circuito è 
$$V_{out} = V_{C,A}(T_0)+V_{A,B}(T_x)+V_{B,A}(T_0)+ V_{A,C}(T_0) = V_{A,B}(T_x)+V_{B,A}(T_0) $$
Il potenziale $V_{B,A}(T_0)$ è una costante: il processo di compensazione di questo offset è detto \textit{cold-junction compensation} (perché $T_0$ è spesso mantenuta costante a $0\,^oC$ in un bagno di acqua e ghiaccio).

Esistono molte coppie di metalli che hanno dei coefficienti di Seebeck ($\nicefrac{dV(T)}{dT}$) perlopiù costanti (quindi, la funzione di trasduzione è perlopiù lineare): le più diffuse ed economiche sono le termocoppie $J$ (ferro e costantana) e $K$ (chromel e alumel), per le basse temperature sono più indicate le $E$ (chromel e costantana) e le $T$ (rame e costantana), per le alte le $B$ (platino+30\% rodio e platino+6\% rodio).

\paragraph{Condizionamento} Il segnale in uscita è già un segnale di tensione: andrà misurato senza consumare corrente. Per questo si utilizzano gli amplificatori per strumentazione (pagina~\pageref{fig:instramp}), con guadagno regolabile scegliendo la resistenza esterna $R_G$ per adattarlo al range di ingresso dell'ADC.

\subsection{RTD}
Gli RTD (\textit{Resistance Temperature Detector}) sono dei sensori di temperatura che sfruttano il principio fisico della termoresistenza: la resistività del materiale è funzione della temperatura.
$$R(T)=R_0(1+\alpha(T-T_0))$$
Le tecnologie più diffuse sono basate sul platino e sono la \textit{Pt-100} (100\,$\Omega$ a 0$\,^o$C) e la \textit{Pt-1000} (1000\,$\Omega$ a 0$\,^o$C). Sono sensori più costosi delle termocoppie e hanno meno range, ma sono molto più precise.

\paragraph{Condizionamento}
\begin{figure} \centering
	\caption{Kelvin connection per il pilotaggio in corrente (\textit{current}) e la misurazione della differenza di tensione (\textit{sense}) introdotta da un RTD}
	\label{fig:kelvinconn}
	\ \newline 
	\includegraphics[width=0.6\textwidth]{img/kelvinconn.pdf}
\end{figure}
Gli RTD sono trasduttori resistivi, devono essere pilotati in corrente, per misurare la tensione e ottenere $R=\nicefrac{V}{I}$\,. Per evitare di avere errori dovuti alla resistenza dei conduttori si usa la \textit{Kelvin connection} (\textit{current-sense}). La corrente che passa attraverso il RTD è tutta la corrente $I$ del generatore: la differenza di potenziale ai capi è $R(T)\,I$\,. Nei conduttori che portano all'uscita non passa corrente, perché sono collegati ad un amplificatore per strumentazione, per cui la resistenza di tali conduttori non provoca cadute di tensione.

\subsection{Sensori Integrati}
Esistono anche sensori di temperatura a semiconduttore disponibili come circuiti integrati. Ad esempio l'Analog Devices AD592 misura da -40 a +120$\,^o$C con una sensibilità di 1$\,\nicefrac{\mu A}{K}$\,. 

\paragraph{Condizionamento} Per non necessitare di un'alimentazione precisa e poter reggere cavi lunghi, l'uscita dei sensori integrati è solitamente in corrente: per misurarla viene presa la differenza di potenziale ai capi di una resistenza nota.

\paragraph{Applicazione} Spesso i sensori di temperatura integrati vengono usati per misurare la temperatura di riferimento della \textit{cold-junction} di una termocoppia: in questo modo si ottengono misure con il range della termocoppia e la precisione dell'integrato.

\section{Sensori di Forza e Deformazione}
L'estensimetro (\textit{strain gauge}) misura la deformazione subita dal conduttore: per la legge di Hooke, la forza è proporzionale all'allungamento ($F=-K\,\Delta x$). La resistenza di un conduttore è determinata dalla resistività dl materiale e dalle sue caratteristiche geometriche ($R=\rho\,\nicefrac{l}{S}$). Per una serpentina ($l \gg S$) la variazione della sezione è trascurabile, quindi
$$ \frac{\Delta R}{R} = K\ \frac{\Delta l}{l} $$
Il coefficiente $K$ dipende dal materiale.

\paragraph{Condizionamento}
\begin{figure} \centering
	\caption{Ponte di Wheatstone per misurare la resistenza di un estensimetro}
	\label{fig:wheatstone}
	\ \newline 
	\includegraphics[width=0.4\textwidth]{img/wheatstone.pdf}
\end{figure}
Il condizionamento di questo trasduttore resistivo è reso difficoltoso dal fatto che stiamo misurando variazioni piccole della resistenza. Viene usato un ponte di Wheatstone (figura~\ref{fig:wheatstone}). Definiamo la resistenza dell'estensimetro $R_E = R+\Delta R$, dove la differenza rispetto alle altre tre resistenze è dovuta alla deformazione. Per il principio del partitore resistivo, la tensione in uscita è
$$ V_{out} = V_{1}-V_{2}=V_{ref}\frac{R}{2R}-V_{ref}\frac{R}{2R+\Delta R} = V_{ref}\frac{2+\nicefrac{\Delta R}{R}-2}{2\left(2+\nicefrac{\Delta R}{R}\right)} $$
Abbiamo detto che stiamo misurando variazioni piccole, per cui
$$ \frac{\Delta R}{R} \ll 1 \Rightarrow V_{out} \approx \frac{\Delta R}{4R}\,V_{ref} $$
Per assicurare che la resistenza di partenza sia uguale alle resistenze degli altri resistori, gli altri resistori sono lo stesso modello di estensimetro, ma scollegato.

Questa modalità di condizionamento prevede un'alimentazione in tensione costante e un uscita ad alta impedenza (amplificatore per strumentazione).

\section{Sensori di Vibrazione Meccanica}
I sensori di vibrazione meccanica possono essere suddivisi a seconda della grandezza misurata per valutare la vibrazione, secondo l'applicazione (in particolare il range di frequenze) e secondo il principio di funzionamento.\begin{itemize}
	\item Grandezza\begin{itemize}
		\item Posizione (es. sensori inerziali)
		\item Velocità (es. sensori magnetodinamici)
		\item Accelerazione (es. accelerometri)
	\end{itemize}
	\item Frequenza\begin{itemize}
		\item Accelerometri, sismografi, geofoni: $f < 1\,kHz$
		\item Microfoni: $20\,Hz < f < 20\,kHz$
	\end{itemize}
	\item Principio di funzionamento\begin{itemize}
		\item Magnetodinamici, induttivi (es. geofoni, sismografi, microfoni)
		\item Piezoelettrici (es. microfoni, accelerometri)
		\item Capacitativi (es. microfoni, MEMS)
	\end{itemize}
\end{itemize}

\subsection{Sensori Magnetodinamici}
I sensori magnetodinamici sono impiegati come microfoni (microfoni dinamici), come pick-up e come geofoni. Esistono anche attuatori magnetodinamici, che sono impiegati come altoparlanti o vibratori.

I sensori magnetodinamici sono costituiti da una bobina solidale con una membrana (diaframma), mossa dalla vibrazione. Un magnete permanente è all'interno della bobina: il movimento di questa causa il relativo movimento del campo magnetico nel conduttore, generando corrente.
$$ \Phi_{B}(t) = \left| \overrightarrow{B} \right|\,S\cdot x(t) $$
$$ V(t) = -\frac{d\Phi_{B}(t)}{dt} = \left| \overrightarrow{B} \right|\,S\cdot \frac{dx(t)}{dt} $$
$$ I(t) = \frac{\int_{-\infty}^{t} V(\tau) d\tau}{L} = \frac{| \overrightarrow{B} |\,S}{L}\cdot x(t) $$
L'intensità è proporzionale alla posizione del diaframma e la tensione velocità ($\nicefrac{dx(t)}{dt}$).

L'effetto magnetodinamico funziona anche nel modo inverso: una corrente o una tensione nella bobina ne provoca lo spostamento. Tale principio viene sfruttato dagli attuatori.

\subsubsection{Microfono Dinamico}
Un microfono dinamico è un trasduttore della forma di un'onda di pressione nell'aria: tale onda provoca lo spostamento del diaframma, proporzionale alla pressione. Per ricostruire la forma d'onda, quindi, siamo interessati all'intensità di corrente in uscita dal trasduttore.

\paragraph{Condizionamento} Nel caso del microfono dinamico, bisogna amplificare un segnale di corrente. Inoltre, siamo interessati solo allo scostamento dalla posizione a riposo del diaframma, non alla posizione assoluta: in termini di corrente, siamo interessati alla componente alternata e non alla componente continua. Questo si ottiene tramite \textit{AC coupling}: ciò consiste nel collegare l'uscita del sensore ad un condensatore, in modo che la componente media sia compensata dalla carica di tale condensatore. L'intensità di corrente (alternata) risultante, può venire poi amplificata per essere mandata all'ADC (che deve essere una \textit{current-sensing ADC} come, ad esempio, la Texas Instruments INA226).

\subsection{Sensori Piezoelettrici}
I cristalli con reticolo asimmetrico sono soggetti all'effetto piezoelettrico: in seguito a compressione o estensione, i poli positivi e negativi del reticolo si avvicinano o allontanano, producendo uno squilibrio di cariche e, quindi, una differenza di potenziale. Il simbolo circuitale di un cristallo piezoelettrico è $\dashv\!\square\!\vdash$\,: il simbolo è simile a quello di un condensatore, perché tali cristalli hanno un'altissima impedenza.

I sensori piezoelettrici più diffusi sono accelerometri, idrofoni, geofoni (con una massa per determinarne sensibilità e risposta in frequenza) e microfoni (cristallo deformato da un diaframma).

\subsection{Sensori Capacitativi}
\begin{figure} \centering
	\caption{Circuito del sensore di un microfono a condensatore: una membrana è mobile e carica, l'altra è fissa e collegata a massa}
	\label{fig:condensermic}
	\ \newline 
	\includegraphics[height=0.8\textwidth,angle=270]{img/condensermic.pdf}
\end{figure}
I sensori capacitativi sono composti da due membrane, una fissa e una mobile, che costituiscono le armature di un condensatore.

\subsubsection{Microfono a Condensatore}
Nel microfono a condensatore (figura~\ref{fig:condensermic}), la membrana mobile è polarizzata (caricata a carica costante) e quella fissa è collegata a massa e perforata (per consentire il passaggio dell'aria): la pressione acustica muove la membrana mobile e varia la capacità del condensatore. 
Il microfono a elettrete, invece, ha come membrana mobile un elettrete, che è composto da un materiale con una carica permanente e non ha, quindi, bisogno di alimentazione per la polarizzazione.

\paragraph{Condizionamento}
La grandezza da misurare è la capacità, quindi, bisogna condizionare in alta impedenza d'ingresso. Spesso si usa l'\textit{AC coupling} perché la grandezza a cui si è interessati è la differenza di capacità, misurata eliminando la componente diretta. Per soddisfare entrambi i requisiti si inserisce un condensatore aggiuntivo in serie al microfono.

\subsubsection{Sensori di Tocco e Pressione}
I sensori capacitativi sono usati anche come \textit{touch sensor} negli schermi tattili: infatti, la presenza di un dito varia la costante dielettrica dei condensatori nella zona interessata dello schermo, che è composto da una matrice di condensatori. Possono essere usati anche come sensori di pressione in configurazione a tre armature in serie: queste determinano due condensatori, uno dei quali viene compresso dalla pressione da misurare e l'altro che costituisce la tara.

\subsubsection{Sensori di Livello}
\begin{figure} \centering
	\caption{Sensore capacitivo per la misurazione del livello di liquido in un serbatoio}
	\label{fig:capacitivelevel}
	\ \newline 
	\includegraphics[width=0.4\textwidth]{img/capacitivelevel.png}
\end{figure}
Un'altra applicazione è la misurazione del livello di un liquido in un serbatoio (figura~\ref{fig:capacitivelevel}): le armature del condensatore sono sulle facce del serbatoio e, a seconda del livello del liquido, una parte della loro superficie è coperta da liquido e un'altra dall'aria. Tale rapporto determina la costante dielettrica del condensatore: per tarare correttamente la misura, la capacità viene confrontata con la capacità di un condensatore di riferimento che è sempre completamente immerso nel liquido. 

\subsubsection{Misurazione della Capacità} 
La misurazione della capacità può avvenire per mezzo di un \textit{charge amplifier} per misurare valori statici della capacità, mentre l'eccitazione con corrente alternata è molto più sensibile a piccole variazioni di capacità.

\section{Sensori di Luminosità}
\subsection{Fotodiodi}
I fotodiodi sono diodi sensibili alla luce nei quali i fotoni causano la generazione di una coppia elettrone-lacuna: la corrente generata è in direzione inversa alla polarizzazione del diodo.

\paragraph{Condizionamento} 
\begin{figure} \centering
	\caption{Amplificazione a transimpedenza di un fotodiodo in configurazione fotovoltaica e in configurazione fotoconduttiva}
	\label{fig:fotodiode}
	\ \newline 
	\includegraphics[width=0.33\textwidth]{img/fotodiodFV.pdf}
	\includegraphics[width=0.33\textwidth]{img/fotodiodFC.pdf}
\end{figure}
Il condizionamento può essere effettuato con un amplificatore a transimpedenza (figura~\ref{fig:fotodiode}): in configurazione fotovoltaica è molto sensibile, ma lento, mentre in configurazione fotoconduttiva è meno sensibile, ma più veloce (utile, per esempio, per le fibre ottiche).

\subsection{Fotoresistenza}
Le fotoresistenze sono resistori con resistenza proporzionale alla luce assorbita: sono sensori molto lenti, utili solo per misure statiche, ma che hanno un enorme range di linearità (anche di 5 ordini di grandezza).
\paragraph{Condizionamento} Il condizionamento può essere fatto con un partitore resistivo perdendo molta della linearità, oppure pilotando il resistore in intensità: in questo modo si può anche utilizzare un amplificatore logaritmico per effettuare una trasposizione su scala logaritmica (es. dB), utile per range così ampi.

\section{Sensori MEMS}
I MEMS (\textit{Micro Electro-Mechanical Systems}) sono sistemi elettromeccanici miniaturizzati (con dimensioni sulla scala dei micrometri) stampati direttamente su un chip. Oltre ad applicazioni di micromeccanica, la tecnologia MEMS permette di avere dei micro-sensori: in un unico chip sono integrati trasduttore, condizionamento e ADC.

\subsection{Accelerometro}
\begin{figure} \centering
	\caption{Struttura di un accelerometro MEMS: lo spostamento della barra solidale con la massa fa variare le capacità dei condensatori}
	\label{fig:memsacc}
	\ \newline 
	\includegraphics[width=0.6\textwidth]{img/memsacc.png}
\end{figure}
Un accelerometro MEMS (figura~\ref{fig:memsacc}) è composto da una massa è libera di muoversi in una direzione, che è sospesa tramite molle e ha delle barre trasversali alla direzione, chiamate anche \textit{fingers}. Queste barre si muovono solidali con la massa, mentre la struttura esterna presenta delle barre fisse, che sono inframezzate da quelle mobili: una barra mobile forma due condensatori con le due diverse barre fisse che la circondano. Spostandosi la massa per inerzia rispetto ad un'accelerazione impressale, le capacità dei condensatori variano di conseguenza (per l'avvicinamento o l'allontanamento delle armature). Un sistema di tre accelerometri così strutturati compone un accelerometro a tre assi.
\paragraph{Condizionamento} Tale accelerometro richiede una misura di capacità, ad esempio tramite trasduzione in tensione con \textit{charge amplifier} o ponte di Wheatstone capacitivo. L'uscita di un accelerometro MEMS può essere analogica o convertita in digitale da un ADC interno.

\subsection{Giroscopio}
I giroscopi MEMS sfruttano l'effetto di Coriolis. Data una massa libera di muoversi in due dimensioni perpendicolari $x$ e $y$, essa viene fatta oscillare nella direzione $x$ con un moto armonico sinusoidale: in assenza di rotazione sul terzo asse perpendicolare $z$, tale movimento non dovrebbe causare alcun moto nella direzione $y$. Per l'effetto di Coriolis, invece, si rileva un'accelerazione sulla direzione non pilotata $y$, proporzionale alla velocità di rotazione attorno all'asse $z$.
Come per gli accelerometri, un sistema composto da tre giroscopi perpendicolari tra loro compone un giroscopio a tre assi.

\subsection{Magnetometro}
I magnetometri MEMS sfruttano la forza di Lorentz: un conduttore di lunghezza $l$, percorso da una corrente di intensità $I$ e immerso in un campo magnetico $\overrightarrow{B}$, subisce una forza di Lorentz $ \overrightarrow{F} = l\cdot \overrightarrow{i}\times \overrightarrow{B} $\,.

Tale forza provoca uno spostamento del MEMS in una direzione non vincolata, che viene rilevato come variazione di capacità tra condensatori.
Un sistema composto da tre magnetometri perpendicolari tra loro compone un magnetometro a tre assi.
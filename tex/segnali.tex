\chapter{Trattamento di Segnali}
Un segnale analogico è una funzione con dominio e codominio continui $$s: \mathbb{R} \rightarrow  \mathbb{R},\ t \mapsto s(t)$$
Un segnale digitale è una funzione con dominio discreto e codominio finito $$x: \mathbb{Z} \rightarrow  A,\ n \mapsto \left\lfloor s(nT)\right\rceil$$
La catena di elaborazione di un segnale analogico in un'architettura digitale prevede:\begin{itemize}
	\item Filtro passa-basso (\textit{anti-aliasing filter})
	\item ADC (\textit{Analog to Digital Converter})
	\item Elaborazione digitale del segnale
	\item DAC (\textit{Digital to Analog Converter})
	\item Filtro passa-basso (\textit{reconstruction filter})
\end{itemize} 

\section{Trasformata di Fourier}
Secondo il teorema di Fourier, ogni segnale periodico continuo a quadrato integrabile sul periodo può essere rappresentato come una sommatoria infinita di seni e coseni, la serie di Fourier.

Il segnale deve essere periodico. $T$ è detto il \textit{periodo} del segnale e il suo reciproco $v_0$ la sua \textit{frequenza fondamentale}
$$s\ |\ \exists T>0 : s(t+T)=s(t)\ \forall t\in\mathbb{R}$$
$$T:=min\graffe{T>0 : s(t+T)=s(t)}$$
$$\nu_0 := \nicefrac{1}{T}$$
Il segnale deve essere continuo ($s\in C^0$) e a quadrato integrabile
$$\int_0^T s^2(t)\,dt\in\mathbb{R}$$
\paragraph{Serie Reale} $$ s(t) = \frac{a_0}{2} + \sum_{n=1}^{+\infty} a_n\cdot cos\left(\frac{2\pi n}{T}\,t\right) + \sum_{n=1}^{+\infty} b_n\cdot sin\left(\frac{2\pi n}{T}\,t\right)$$
I coefficienti sono dati dal prodotto scalare del segnale per le \textit{armoniche} (i seni e coseni con frequenze multiple della fondamentale)
$$a_n = \frac{2}{T}\int_{0}^{T} s(t)\cdot cos\left(\frac{2\pi n}{T}\,t\right) dt $$
$$b_n = \frac{2}{T}\int_{0}^{T} s(t)\cdot sin\left(\frac{2\pi n}{T}\,t\right) dt $$

\paragraph{Serie Complessa} $$ s(t) = \sum_{n=-\infty}^{+\infty} c_n\cdot e^{-i\,\frac{2\pi n}{T}\,t}$$
I coefficienti sono dati dal prodotto scalare del segnale per le \textit{armoniche} (le esponenziali complesse con frequenze multiple della fondamentale)
$$c_n = \frac{2}{T}\int_{0}^{T} s(t)\cdot e^{+i\,\frac{2\pi n}{T}\,t} dt $$

\paragraph{Trasformata} Per segnali non periodici $T=+\infty$. Il limite dei coefficienti della serie di Fourier per $T\rightarrow +\infty$ compongono la trasformata di Fourier, mentre la serie l'antitrasformata
$$ \mathcal{F}\graffe{f(t)}(\nu) = \int_{-\infty}^{+\infty} f(t)\cdot e^{-i\,2\pi\nu\,t}\ dt $$
$$ \mathcal{F}^{-1}\graffe{F(\nu)}(t) = \int_{-\infty}^{+\infty} F(\nu)\cdot e^{+i\,2\pi\nu\,t}\ d\nu $$
La trasformata di Fourier di un segnale reale è un segnale complesso.

\section{Campionamento}
Il campionamento è l'operazione che trasforma un segnale a tempo continuo in un segnale a tempo discreto: l'ampiezza del segnale viene campionata ad intervalli di tempo regolare $T_s=\nicefrac{1}{\nu_s}$ (campionamento uniforme). Questa operazione può essere espressa come il prodotto tra il segnale ed un treno di impulsi di Dirac
$$s_s(t) := s(t)\cdot \sum_{n=-\infty}^{+\infty} \delta(t-nT_s) = \sum_{n=-\infty}^{+\infty} s(nT_s)\cdot \delta(t-nT_s) $$
Lo spettro del segnale campionato è la somma delle repliche di periodo $\nu_s$ dello spettro del segnale
$$S_s(\nu) := S(\nu)\ast \nu_s \sum_{n=-\infty}^{+\infty} \delta(\nu-n\nu_s) = \nu_s \sum_{n=-\infty}^{+\infty} S(\nu-n\nu_s) $$
Se queste repliche spettrali si sovrappongono, avviene il fenomeno dell'\textit{aliasing}. Il teorema del campionamento di Nyquist-Shannon afferma che un segnale può essere ricostruito a partire dai suoi campioni se il suo spettro è limitato ad una frequenza minore della metà della frequenza di campionamento
$$ S(\nu) = 0\ \forall \nu \geq \frac{\nu_s}{2} \Rightarrow no\ aliasing $$

\section{Quantizzazione}
La quantizzazione è l'operazione che mappa il dominio continuo delle ampiezze analogiche in un insieme discreto di ampiezze.
$$ Q: [V_{min}\,, V_{max}] \rightarrow \graffe{v_0,v_1, \dots\, v_{N-1}}, v \mapsto Q(v) = \argmin_{v_i \in \graffe{v_0,v_1, \dots\, v_{N-1}} } |v-v_i| $$
La quantizzazione, a differenza del campionamento, è un processo irreversibile: introduce un rumore nel segnale, chiamato errore di quantizzazione.

Il quantizzatore uniforme divide il dominio in $N$ intervalli con la stessa larghezza
$$ \Delta = \frac{V_{max}-V_{min}}{N} $$
Il valore quantizzato è il punto medio di ogni intervallo
$$ v_i = V_{min}+\frac{V_{max}-V_{min}}{N}\cdot (i+0.5)\hspace{.5in} \forall i\in\nint{0}{N-1} $$

\subsection{Rumore di Quantizzazione}
L'errore di quantizzazione granulare (per input nel dominio previsto) è compreso nell'intervallo $[-\nicefrac{\Delta}{2}, \nicefrac{\Delta}{2}]$\,. Essendo non lineare, la quantizzazione genera armoniche: possiamo misurare la qualità della quantizzazione con la THD (\textit{Total Harmonic Distortion})
$$THD_{dB}=10 \log_{10}\left( \frac{\sum_{i=2}^{+\infty}P_{h_i}}{P_{s}} \right)$$
Dove $P_s$ è la potenza del segnale originale (non quantizzato) e $P_{h_i}$ è la potenza dell'armonica $i$-esima. A livello spettrale il rumore di quantizzazione è \textit{bianco} (costante in ampiezza su tutte le frequenze).

Dato un segnale casuale (uniforme) in ingresso, l'errore di quantizzazione è un processo uniforme sull'intervallo $[-\nicefrac{\Delta}{2}, \nicefrac{\Delta}{2}]$\,. La potenza del rumore di quantizzazione è 
$$ P_Q = \int_{-\nicefrac{\Delta}{2}}^{\nicefrac{\Delta}{2}} p_E(e)\cdot e^2\ de = \frac{1}{3\Delta} \int_{-\nicefrac{\Delta}{2}}^{\nicefrac{\Delta}{2}} 3e^2\ de = \frac{\left[ e^3 \right]_{-\nicefrac{\Delta}{2}}^{+\nicefrac{\Delta}{2}}}{3\Delta} = \frac{2\nicefrac{\Delta^3}{8}}{3\Delta} = \frac{\Delta^2}{12} $$
Consideriamo il dominio centrato sullo zero ($V_{min}=-V_{max}$), la potenza (massima, picco-picco) del segnale è 
$$ P_s = \left( V_{max} \right)^2 = \left( 2^{N-1}\Delta \right)^2 = 2^{2N-2}\Delta^2 $$
Dove $N$ è il numero di bit usati per la quantizzazione, quindi il livelli sono $2^N$ e i picchi sono a $\pm 2^{N-1}\Delta$\,.
Il rapporto segnale-rumore di quantizzazione (\textit{Signal to Quantization Noise Ratio}) è 
$$  SQNR_{dB} = 10\log_{10}\left( \frac{P_s}{P_Q} \right) = 10\log_{10}\left( \frac{2^{2N-2}\Delta^2}{\nicefrac{\Delta^2}{12}} \right) = 10\log_{10}\left( 3\ 2^{2N} \right) $$
$$ SQNR_{dB} \approx 4.77 + 20\log_{10}(2^N) = 4.77 + 20\frac{N}{\log_{2}(10)} \approx 4.77 + 6.02\,N $$

\section{DAC}
Un convertitore digitale-analogico (\textit{Digital to Analog Converter}) è un circuito che genera in uscita una tensione (analogica) proporzionale al numero binario codificato dagli ingressi (digitali)

\subsection{Sommatore}
\begin{figure} \centering
	\caption{DAC sommatore, la tensione in uscita è la somma delle tensioni proporzionali alle potenze di due associate ai bit}
	\label{fig:dac}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/DAC.pdf}
\end{figure}
Per effettuare una conversione da analogico a digitale è possibile utilizzare un sommatore (sommatore invertente a pagina~\pageref{fig:invsum}) che abbia un ingresso per ogni bit: il bit $i$-esimo di $N$ ha $2^{N-i-1}$ resistenze $R$ collegate in serie, la tensione in uscita è
$$ V_{out} = -R \sum_{i=0}^{N-1} \frac{V\ b_i}{2^{N-i-1}R} = -2^{N-1}\,V\sum_{i=0}^{N-1} 2^i\ b_i $$
Quindi è proporzionale al numero codificato in binario $\sum_{i=0}^{N-1} 2^i\,b_i$\,.
Questo modello ha un problema grave, il range di resistenze è enorme: ad esempio, per 16 bit il range va da $R$ a $2^{16}\,R$, quindi necessiterebbe di una precisione di 5 cifre significative.

\subsection{R/2R}
\begin{figure} \centering
	\caption{DAC con schema R/2R. Sono evidenziate le resistenze equivalenti delle resistenze in parallelo che portano a massa}
	\label{fig:r2r}
	\ \newline 
	\includegraphics[width=0.8\textwidth]{img/R2R.pdf}
\end{figure}
Lo schema più usato per la conversione da digitale a analogico è lo schema R/2R: esso prende il nome dal fatto che sono necessari resistori di valore $R$ e $2R$, limitando il range che si aveva per lo schema precedente.

Un convertitore a $N$ bit ha la corrente di riferimento $V_{ref}$ in ingresso su $N-1$ resistori $R$ in serie e, in serie a questi, un resistore $2R$ (o due resistori $R$). Ad ogni terminale dei resistori $R$ vengono posti dei resistori $2R$, che portano corrente ad altrettanti ($N$) switch attivati dai bit di codifica. Se il bit è a zero, la corrente viene mandata a terra, se vale uno viene mandata all'ingresso di un sommatore invertente con resistenza in retroazione $R$.

Consideriamo i resistori $2R$ in coda e l'ultimo resistore $2R$ che porta corrente allo switch (rettangolo interno tratteggiato in figura~\ref{fig:r2r}), abbiamo due resistori $2R$ in parallelo: la loro resistenza equivalente è di
$ R_{eq}=\frac{1}{\nicefrac{1}{2R}+\nicefrac{1}{2R}}=R $\,. Ma, allora, la resistenza equivalente di questo sistema di resistenze, in serie con un resistore $R$, è di $2R$. Il che vuol dire che la porzione di circuito che comprende gli ultimi due switch e la relativa porzione di resistori in serie (secondo rettangolo dall'interno interno in figura~\ref{fig:r2r}) è equivalente a due resistori $2R$ in parallelo. Per ricorsione, tutti i sistemi formati da un resistore $2R$ che porta ad uno switch e la porzione di resistori in serie a valle (rettangoli tratteggiati in figura~\ref{fig:r2r}) hanno una resistenza equivalente di $2R$.

Calcoliamo ora il potenziale ai terminali dei resistori $2R$. Sul primo il potenziale è $V_N=V_{ref}$. Il secondo è il punto intermedio di un partitore resistivo che porta da massa a $V_{ref}$ e ha una resistenza a monte di $R$ e una a valle di $R$ (due resistori $2R$ in parallelo). Il potenziale sul secondo terminale sarà, quindi, $V_{N-1}=V_N\frac{R}{R+R}=\nicefrac{1}{2}V_{ref}$. In generale, per ogni terminale $k$-esimo vale 
$$ V_{N-k} = 2^{-k}\ V_{ref}\hspace{.8in} \forall k\in\nint{0}{N-1} $$
La corrente che attraversa lo switch (qualora attivo) è 
$$ I_{N-k} = \frac{V_{N-k}}{2R} = \frac{V_{ref}}{2R}2^{-k}$$
Il bit $i$-esimo controlla il terminale $(N-i-1)$-esimo, la corrente che arriva al sommatore è, quindi 
$$ I_{out} = \sum_{i=0}^{N-1} I_{N-i-1}\cdot b_i = \sum_{i=0}^{N-1} \frac{V_{ref}}{2R}2^{i+1-N}\cdot b_i = \frac{2^{-N}\,V_{ref}}{R} \sum_{i=0}^{N-1} 2^i\cdot b_i $$
La tensione in uscita è 
$$ V_{out} = -I_{out}R = -2^{-N}\,V_{ref} \sum_{i=0}^{N-1} 2^i\cdot b_i $$
Che è proporzionale al numero binario codificato. 

Questo schema ha i vantaggi di ridurre il range di resistenze utilizzate, richiedendo una precisione minore, e di utilizzare un numero di resistori $O(N)$ (contro $O(2^N)$ dello schema precedente).

\subsection{PWM}
La \textit{Pulse Width Modulation} è una tecnica di conversione da digitale ad analogico che consiste nel modulare la larghezza di un'onda quadra nel tempo: se in ogni periodo $T$ l'onda quadra vale \bittt{1} per un tempo $T_1$, allora il \textit{duty cycle} (la larghezza dell'impulso) è $\nicefrac{T_1}{T}$. Se questo segnale digitale viene fatto passare attraverso un filtro passa basso (media mobile), il valore del segnale nel punto medio del periodo sarà
$$ \overline{f} = \frac{1}{T}\int_{0}^{T} f(t)\cdot dt = \frac{T_1\cdot 1+(T-T_1)\cdot 0}{T}=\frac{T_1}{T}$$
Controllando il duty cycle è possibile generare un segnale analogico senza aver bisogno di hardware specifico (solo un filtro passa basso): infatti il segnale in uscita è digitale (onda quadra) e può essere generato dall'hardware per il normale I/O.

Inoltre, in molte applicazioni non è necessario il filtraggio: ad esempio, se si desidera utilizzare solo una frazione della potenza di un dispositivo ( una stufa, un led, \dots ) è possibile utilizzare la PWM per attivarlo alla massima potenza per una frazione del tempo, ottenendo un effetto analogo.

\section{ADC}
Un convertitore analogico-digitale (\textit{Analog to Digital Converter}) è un circuito che codifica digitalmente la tensione analogica in ingresso.

\subsection{Single Slope}
\begin{figure} \centering
	\caption{Circuito di un ADC \textit{single slope}, l'operazionale in alto è il comparatore, quello in basso l'integratore. I moduli a destra sono un contatore e uno \textit{shift register}}
	\label{fig:singleslope}
	\ \newline 
	\includegraphics[width=0.8\textwidth]{img/SingleSlope.pdf}
\end{figure}
Il convertitore analogico-digitale a rampa singola (figura~\ref{fig:singleslope}) basa il proprio funzionamento sull'amplificatore integratore (pagina~\pageref{fig:integrator}): per tensioni in ingresso costanti, la tensione in uscita da tale amplificatore è l'inverso della tensione in ingresso dopo un tempo $T=RC$\,, aumentando linearmente. Dato che la tensione in ingresso all'integratore è -$V_{ref}$, la tensione in uscita è
$$ V_{\int}(t) = \frac{t}{RC}V_{ref} $$
Quando tale tensione è uguale a $V_{in}\,$, un altro amplificatore operazionale, usato come comparatore, cambierà il proprio valore dalla tensione minima alla massima. In termini digitali, l'uscita dell'operazionale è \texttt{V\_s >= V\_in}\,.

Nel frattempo, un contatore conta il tempo passato. Il comparatore scatta dopo un tempo proporzionale alla tensione in ingresso
$$ T_x = \frac{RC}{V_{ref}}V_{in} $$
Misurando il tempo $T_X$ con il contatore, possiamo ricavare la tensione 
$$ V_{in} = \frac{V_{ref}}{RC}T_x $$
Infatti, allo scattare del comparatore (fronte di salita), il valore in uscita dal contatore viene salvato sullo \textit{shift register} e diventa il valore del nuovo campione. Contemporaneamente, viene chiuso un MOS, che scarica il condensatore dell'integratore, resettandolo e predisponendolo ad acquisire una nuova misura. Essendo l'uscita dell'integratore pari a zero, l'uscita del comparatore si riporta a zero: su tale fronte di discesa viene resettato il contatore; inoltre, si riapre il MOS, permettendo la carica del condensatore.

Il nome deriva dal fatto che la tensione in uscita dall'integratore cresce (come una rampa) fino ad avere un valore uguale a quello in ingresso.

Questo schema ha prestazioni instabili, dovute ai \textit{calibration drift} causati da fluttuazioni del valore della resistenza con la temperatura. Inoltre, il campionamento non è uniforme, ma l'intervallo di campionamento dipende linearmente dal valore in ingresso. Questo porta a conversioni adatte solo a misure statiche.

\subsection{Dual Slope}
\begin{figure} \centering
	\caption{Circuito di un ADC \textit{dual slope}. L'unità di controllo (in viola) gestisce i \textit{flag} degli \textit{switch} (linee tratteggiate)}
	\label{fig:dualslope}
	\ \newline 
	\includegraphics[width=\textwidth]{img/DualSlope.pdf}
\end{figure}
L'ADC a doppia rampa sfrutta lo stesso principio di quello a rampa singola, estendendolo. La logica di controllo gestisce il clock, i bit di \textit{clear} e i due bit che pilotano gli \textit{switch}.

All'inizio, sul primo operazionale (integratore) lo \textit{switch} seleziona $V_{in}$ e sul secondo (comparatore) -$V_{ref}$\,. La tensione in uscita dall'integratore aumenta proporzionalmente a -$V_{in}$\,: il comparatore scatta quando tale tensione raggiunge -$V_{ref}$
$$ -V_{ref}=-\frac{V_{in}}{RC}T_x $$
A questo punto, l'unità di controllo inverte gli \textit{switch}: ora la tensione in uscita dall'integratore aumenta proporzionalmente a $V_{ref}$ e il comparatore scatta a $0$
$$ 0 = -V_{ref}+\frac{V_{ref}}{RC}T_{ref} \Rightarrow \frac{V_{in}}{RC}T_x = \frac{V_{ref}}{RC}T_{ref} \Rightarrow V_{in} = V_{ref} \frac{T_{ref}}{T_x} $$

Il nome deriva dal fatto che la tensione in uscita dall'integratore prima cresce e poi decresce (come una rampa che sale e scende), andando da zero a -$V_{ref}$ e viceversa.

Con questo meccanismo, la misura della tensione non è più dipendente dai valori di resistenza e di capacità dell'amplificatore integratore, rendendola molto più precisa (fino a circa 24 bit). Il tempo impiegato è proporzionale all'ampiezza massima, quindi è $O(2^N)$ (dove $N$ è il numero di bit), il che lo rende adatto a misure statiche con frequenza fino ad un massimo di $100\,Hz$\,.

\subsection{SAR}
\begin{figure} \centering
	\caption{Circuito di un SAR ADC: la ricerca dicotomica è effettuata comparando con un operazionale la tensione in ingresso con una tensione nota generata da un DAC }
	\label{fig:sar}
	\ \newline 
	\includegraphics[width=0.7\textwidth]{img/SAR.pdf}
\end{figure}
Il \textit{Successive Approximation Register} ADC trova il valore della tensione in ingresso tramite ricerca dicotomica: i valori con cui la tensione in ingresso viene confrontata sono determinati digitalmente da un unità di controllo (il SAR) e convertiti in tensione da un DAC. Un amplificatore operazionale in configurazione di comparatore permette di capire se la tensione in ingresso è maggiore o minore di quella di test. Una volta determinato il valore di tensione più vicino, il valore viene portato sullo \textit{shift register}.

La velocità di conversione dipende dalla complessità della ricerca dicotomica, che è logaritmica nel numero di valori: è, quindi, lineare nel numero di bit di quantizzazione ($O(\log(2^N))=O(N)$).

Questi convertitori hanno buone risoluzioni (fino a 18 bit) e velocità (con frequenze di campionamento fino a un ordine di 1\,MHz) ed è molto utilizzato dalla \textit{Texas Instruments}.

\subsection{Flash}
\begin{figure} \centering
	\caption{Circuito di un ADC flash a 8\,bit, costituito da un partitore resistivo e da un banco di comparatori, codificato in priorità }
	\label{fig:flash}
	\ \newline 
	\includegraphics[width=\textwidth]{img/Flash.pdf}
\end{figure}
Il convertitore \textit{flash} ha un principio di funzionamento molto semplice: la tensione di riferimento è collegata a terra attraversando $2^N$ resistori (dove $N$ è il numero di bit), subendo una caduta di potenziale di $2^{-N}\,V_{ref}$ ad ogni resistore (per il principio del partitore resistivo).

Ai terminali dei resistori, il potenziale viene comparato tramite amplificatore operazionale alla tensione in ingresso. L'uscita dell'operazionale $i$-esimo (dove l'operazionale 1 è quello prossimo alla terra) è 
$$ V_i = \begin{cases*}
V_{ref} & $V_{in} > \frac{i}{2^N}\,V_{ref}$\\
0 & $V_{in} \leq \frac{i}{2^N}\,V_{ref}$
\end{cases*} $$
In termini digitali, ha il valore logico dell'espressione $V_{in} > i\,2^{-N}\,V_{ref}$\,. Il valore della tensione è approssimato scegliendo, tra gli op-amp attivati, quello con il maggiore valore di tensione associato (meno metà della caduta di tensione per ottenere il punto medio dell'intervallo)
$$ V_{in} \approx \max_{i\in\nint{1}{N}:V_i=V_{ref}} \frac{i-0.5}{2^N}\,V_{ref}$$
Possiamo ottenere la codifica binaria della tensione utilizzando un \textit{priority encoder}: questo modulo ha $M$ ingressi e codifica in $\log_2{M}$ bit il numero massimo associato ad un ingresso che è a \bittt{1}\,.

Questo schema è estremamente veloce (con frequenze di campionamento anche superiori ai 100MHz), ma ha un costo esponenziale dei materiali: per un convertitore a $N$ bit, servono $2^N$ op-amp e resistori.

\subsection{Subranging Pipeline}
Uno schema che ottimizza i costi del convertitore flash è il convertitore \textit{subranging pipeline}. Questo schema suddivide la conversione in più moduli, ognuno con un \textit{sample-and-hold}, un ADC flash e un DAC.
Il blocco $i$-esimo ha il seguente funzionamento
\begin{itemize}
	\item L'ADC flash converte a $N_i$ bit la tensione dal \textit{sample and hold} $i$-esimo
	\begin{itemize}
		\item Il primo \textit{sample and hold} contiene il valore di tensione in ingresso
	\end{itemize}
	\item Il DAC produce il valore di tensione codificato dall'ADC 
	\item L'errore di quantizzazione viene calcolato analogicamente, ad esempio, tramite un amplificatore differenziale (pagina~\pageref{fig:diffamp})
	\item Tale residuo viene amplificato e memorizzato nel \textit{sample and hold} successivo ($i+1$-esimo)
\end{itemize}
La concatenazione delle parole codificate dagli ADC flash costituisce la parola complessiva in uscita.

Se il numero di bit è uguale in tutti i $K$ blocchi, il numero di op-amp richiesti è $K\ 2^{\nicefrac{N}{K}}$: il risparmio è dell'ordine di $\nicefrac{2^K}{K}$\,.

Ogni conversione avviene alla velocità dell'ADC flash: il tempo di conversione di un campione viene moltiplicato per $K$. Tuttavia, la frequenza di campionamento rimane invariata, perché quando il blocco $i$ sta processando il campione $\tau$, il blocco $i-1$ è libero di processare il campione $\tau+1$ in \textit{pipeline}, per l'appunto.

Il nome \textit{subranging} si riferisce al fatto che ogni blocco converte una porzione diversa del \textit{range} dinamico del segnale.

\subsection{Modulazione Delta}
\begin{figure} \centering
	\caption{Circuito di un modulatore delta: le uscite binarie vengono integrate (e amplificate per ottenere la funzione integrale scalata correttamente e con il giusto segno). L'integrale viene comparato con l'ingresso (quantizzazione della differenza) e salvato sincronamente nel latch}
	\label{fig:delta}
	\ \newline 
	\includegraphics[width=0.6\textwidth]{img/DeltaMod.pdf}
\end{figure}
La modulazione $\Delta$ è una tecnica di conversione da analogico a digitale che consiste nel codificare il segnale quantizzando la sua derivata ad 1\,bit.

La derivata quantizzata del segnale è ottenuta con un comparatore che confronta il segnale in ingresso con il valore del segnale al campione precedente. La ricostruzione dei campioni è effettuata con un amplificatore integratore.

Un primo problema del modulatore delta è il dimensionamento della scalinata con cui approssimare il segnale. La frequenza di campionamento $\nu_s$ e l'ampiezza del $\Delta$ devono essere tali che la derivata del segnale non sia maggiore in modulo di $\nu_s\,\Delta$: se così non fosse, le variazioni del segnale sarebbero troppo ampie o veloci per essere codificate. Inoltre, tutti gli errori di quantizzazione vengono accumulati nel tempo (per effetto dell'integratore).

\subsection{Modulazione Sigma-Delta}
\begin{figure} \centering
	\caption{Circuito di un modulatore $\Sigma$-$\Delta$. Andamento nel tempo dei segnali nel circuito: si noti che l'integrale nel grafico non è invertito. Inoltre, per chiarezza della visualizzazione, tutti grafici - con l'esclusione dell'uscita digitale - sono ottenuti con una frequenza di clock estremamente bassa }
	\label{fig:sigmadelta}
	\ \newline 
	\includegraphics[width=\textwidth]{img/SigmaDeltaMod.pdf}
	\includegraphics[width=\textwidth,trim={ 1.33in 0.5in 1.33in 0.5in },clip]{pyplots/sigmadeltasignals.pdf}
\end{figure}
Il modulatore $\Sigma$-$\Delta$ rappresenta l'evoluzione del modulatore delta, che ne migliora le prestazioni evitando di accumulare errori nel tempo. 

In questo schema, la differenza non viene fatta rispetto al segnale ricostruito, ma rispetto alla modulazione binaria: il segnale risultante è, quindi, l'input con un offset di -$V_{max}$ o -$V_{min}$ a seconda del fatto che l'ultimo bit codificato sia rispettivamente \bittt{1} o \bittt{0}.

Tale differenza viene integrata da un amplificatore integratore (la differenza è svolta dallo stesso amplificatore, portando i due operandi, di cui uno invertito, in parallelo sull'ingresso), che ne inverte anche il segno. Tramite un amplificatore comparatore (collegato a massa sul terminale invertente), l'integrale invertito viene quantizzato: l'uscita è \bittt{1} se l'integrale è minore di zero, mentre è \bittt{0} se è positivo.

Un flip-flop memorizza sincronamente i valori quantizzati: la frequenza di clock è un multiplo della frequenza di campionamento desiderata. L'uscita \texttt{Q} viene inviata al primo amplificatore dopo essere amplificata da un amplificatore non-invertente (che agisce da DAC a 1\,bit): sarà il sottraendo della differenza.

L'uscita $\overline{\texttt{Q}}$ è il valore dell'integrale (non invertito) quantizzato, viene inviata ad un filtro digitale. La somma dei bit costituisce il \textit{duty cycle} del segnale: viene, quindi, utilizzata per calcolare il numero binario che codifica l'ampiezza. Per questo il clock del bistabile deve avere una frequenza multipla della frequenza di campionamento: se la frequenza è $k\cdot \nu_s$ avremo una somma che può variare tra $0$ e $k$. 

Il filtro può anche considerare più campioni o il circuito usare più integratori in cascata per abbattere ulteriormente il rumore.

Si noti che la codifica in uscita non dipende dalla ricostruzione del segnale negli istanti precedenti: per cui, gli errori non si propagano, ma rimangono localizzati.

I convertitori $\Sigma$-$\Delta$ hanno frequenze fino ad un ordine di 1\,MHz e risoluzioni fino a 24\,bit. Per le caratteristiche del filtro, più abbassiamo la frequenza di campionamento, meno rumore abbiamo e, quindi, più bit puliti. Viene chiamato ENOB (\textit{Effective Number Of Bits}) la risoluzione effettiva del convertitore, che è funzione della frequenza di campionamento: ad esempio l'Analog Devices ADS1256 è un convertitore $\Sigma$-$\Delta$ con ENOB di 19.8\,bit a 30\,kH e 24.2\,bit a 30\,Hz (addirittura 25.3\,bit a 2.5\,Hz).
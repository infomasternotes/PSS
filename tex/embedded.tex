\chapter{Sistemi Embedded}
I sistemi embedded sono moduli prefabbricati e riprogrammabili che contengono già molti componenti utili per le varie applicazioni per cui sono stati progettati (processore, periferiche I/O, \dots). Hanno il vantaggio di una maggiore semplicità d'uso (devono essere solo riprogrammati, non progettati da zero) e velocità di programmazione, ma risultano meno performanti (le operazioni sono svolte dal software, non dall'hardware) più costosi e più ingombranti.

\paragraph{Processori} I processori presenti nei sistemi embedded sono raramente dei GPP (\textit{General Purpose Processor}, al più sono comunque RISC e non CISC), ma sono più comunemente degli ASP (\textit{Application-Specific Processor}): gli ASP più diffusi sono i DSP, i \uC\ e i Network Processors.

\section{DSP}
I DSP (\textit{Digital Signal Processor}) sono processori progettati per l'elaborazione di flussi di segnali digitali, in particolare in applicazioni in cui sia importante avere una bassissima latenza.

La maggior parte delle operazioni tipiche di elaborazione dei segnali (somma vettoriale, variazione di scala, sommatorie, sommatorie quadratiche, prodotto scalare, prodotto di convoluzione, trasformata di Fourier) prevede il calcolo di prodotti e somma cumulativa: viene implementato, quindi, un hardware che consenta di effettuare questa operazione di MAC (\textit{Multiply and ACcumulate}) in un solo colpo di clock (figura~\ref{fig:mac}) $$y_{t+1}=y_{t}+A\cdot x_{t+1}$$
\begin{figure} \centering
	\caption{Esempio di hardware per MAC}
	\label{fig:mac}
	\ \newline 
	\includegraphics[height=0.5\textwidth,angle=90]{img/MAC.pdf}
\end{figure}
\subsection{Architetture Harvard}
Per avere una bassa latenza, la memoria è organizzata secondo un sistema di cache a tre livelli \begin{enumerate}
	\item[L0:] \ \,16\,kB $\sim$ 32\,kB
	\item[L1:] \ \,32\,kB $\sim$ 256\,kB
	\item[L2:] 256\,kB $\sim$ 4\,MB
\end{enumerate}
Inoltre, al contrario delle architetture Von Neumann (Stanford) in cui la memoria è unica per dati e programmi, le architetture Harvard hanno due memorie L0 separate per programmi e dati, così da poter effettuare in un solo colpo di clock il fetch di un istruzione e di un dato in memoria: alcune architetture utilizzano memorie singole ma con doppie porte per poter sfruttare i vantaggi del fetch parallelo, senza dover allocare a priori una parte di memoria per i dati e una parte per i programmi.

Se le architetture Harvard vengono implementate anche in \uC\ e in SoC, sono esclusive dei DSP Analog Devices le architetture SHARC (\textit{Super-Harvard ARChitecture}): in queste architetture la cache L0 è tripla, in modo da poter accedere parallelamente a programmi, dati e costanti.

\subsection{Virgola}
Una scelta molto importante quando si tratta di progettare o scegliere un DSP è se svolgere le elaborazioni in virgola fissa o in virgola mobile. I DSP \textit{fixed point} hanno il vantaggio di non richiedere hardware specifico, perché i conti in virgola fissa possono essere svolti da una normale ALU. I DSP \textit{floating point} necessitano di una FPU specifica, ma riescono a essere più precisi e a permettere una più semplice trattazione di segnali in molti domini. Rimangono, però molto più lenti, infatti, i DSP \textit{fixed point} sono 2 ordini di grandezza più veloci dei DSP \textit{floating point}.

\subsection{Parallelismo}

\subsubsection{Single Instruction Multiple Data}
Le architetture SIMD permettono di svolgere operazioni identiche parallelamente su più dati. Ciò è utile nei DSP, specialmente nel caso di operazioni da svolgere su ogni elemento di un vettore ( prodotto scalare per vettore, somma vettoriale, \dots ). 

\subsubsection{Unità Funzionali}
Alcuni DSP, come la famiglia dei Texas Instruments \textit{TMS320} hanno più unità di calcolo specializzate, che possono essere utilizzate in parallelo: il TMS320-C6000 ha 2 datapath con 4 unità funzionali specializzate ciascuna \\\ \\\begin{tabular}{lll}
	\, & L: & ALU \\
	   & S: & Comparatore (es. \textit{branch}) \\
	   & M: & Moltiplicatore \\
	   & D: & Unità Dati (\textit{load}, \textit{store})
\end{tabular}\\\ \\
Oltre a questa concorrenza, le unità possono avere istruzioni SMID (delle unità del TMS320-C6000, solo le D non ne supportano): ad esempio, possono essere svolte da un unità L o una istruzione su numeri a 32\,bit o due istruzioni su numeri a 16\,bit o addirittura quattro istruzioni su numeri a 8\,bit.

\subsubsection{Very Large Instruction Word}
Le architetture VLIW effettuano il fetch di più istruzioni in parallelo: ad esempio nel TMS320-C6000 vengono utilizzate parole da 256\,bit che contengono 8 istruzioni da 32\,bit. Il LSB di ogni istruzione è il \textit{p-bit}, un flag booleano che indica se l'istruzione a cui appartiene può essere eseguita parallelamente all'istruzione successiva (\bittt{1}) o no (\bittt{0}): l'insieme di istruzioni che possono essere eseguite in parallelo è chiamato \textit{Execute Packet}. Questa concorrenza è esplicita nell'assembly, dove viene indicata con il simbolo \texttt{||}.

\section{\uC}
I microcontrollori sono processori ottimizzati per l'utilizzo in sistemi embedded. Spesso sono distribuiti su board che integrano periferiche aggiuntive (memorie, I/O, JTAG, \dots ), in questo caso si parla più propriamente di SoC (\textit{System on Chip}). Spesso, i microcontrollori dotati di funzionalità di DSP vengono categorizzati come MSP (\textit{Mixed-Signal Processor}).

Ormai, la maggior parte dei SoC è riprogrammabile (oltre che in assembly) in C, mentre per alcuni sono disponibili ambienti che ne consentono la programmazione con linguaggi ancora più di alto livello.

\subsection{Programmazione}
\subsubsection{Special Function Registers}
Oltre ai normali registri, i \uC\ possono accedere a degli \textit{Special Function Register}. Questi registri implementano la comunicazione con le periferiche: essi permettono di specificare la configurazione della periferica associata o di trasmettere o ricevere dati.

\subsubsection{Main}
Nella programmazione di SoC, il main è un processo che viene continuamente reiterato. In esso vanno inserite le procedure a bassa priorità, che non abbiano particolari necessità di sincronizzazione.

\subsubsection{Interrupt}
Una delle specializzazioni dei \uC\ è la gestione degli interrupt: il sistema può essere programmato in modo che la ricezione di un interrupt scateni l'esecuzione di una funzione. Queste funzioni implementano le funzionalità del SoC che hanno dei forti vincoli temporali ( gestione di stream di campioni, \dots ).

\section{Network Processors}
\begin{figure} \centering
	\caption{Architettura tipica di un network processor}
	\label{fig:netp}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/NetworkProcessor.pdf}
\end{figure}
I \textit{Network Processor} sono processori specializzati nella gestione del traffico di rete, impiegati in dispositivi di vario livello ( \textit{hub}, \textit{router}, \textit{gateway}, \dots ). L'architettura tipica (figura~\ref{fig:netp}) prevede che i singoli processori di canale, che si occupano di ricevere e trasmettere dall'interfaccia fisica, comunichino attraverso un bus ad elevata larghezza di banda con buffer e tabelle di lookup, i cui dati possono poi essere esportati su una memoria esterna, e con un processore che si occupa di interfacciarsi con l'host.

\section{JTAG}
Per il testing dei sistemi embedded, le metodologie tradizionali erano due \begin{itemize}
	\item \textit{Edge-connector} test\\Detto anche test \textit{funzionale}, prevede che ci si colleghi al sistema fornendo un insieme di input e osservando gli output, monitorando il funzionamento globale del sistema
	\item \textit{Bed of nails} test\\Detto anche \textit{in-circuit} test, prevede che con un insieme di aghi, collegati ai connettori interni del sistema, si vada a monitorare l'andamento di ogni segnale interno
\end{itemize}
Per sistemi complessi, il test funzionale - in caso di errori - non consente di capire quale componente è difettoso, mentre il test \textit{bed of nails} diventa ingestibile per la numerosità dei componenti ( i connettori tra $n$ componenti sono $O(n^2)$ ).

\paragraph{Boundary Scan}
(IEEE 1149.1) è lo standard per il testing proposto da JTAG (\textit{Joint Test Action Group}): un architettura integrata nel sistema per cui ad ogni pin viene collegata una \textit{boundary scan cell}, che può osservare i dati in transito, oppure forzarli nel sistema. Lo standard prevede:\begin{itemize}
	\item Segnali \begin{itemize}
	\item TDI/TDO (\textit{Test Data In}, \textit{Test Data Out})
	\item TMS (\textit{Test Mode Set})
	\item TCK (\textit{Test Clock})
	\item TRST (\textit{Test Reset}, opzionale)
	\end{itemize} \item Registri \begin{itemize}
	\item \textit{Bypass Register}
	\item \textit{ID Register}
	\item \textit{Instruction Register}
	\item \textit{Boundary Scan Register} (\textit{Scan Register Chain})
\end{itemize} \item TAP (\textit{Test Access Port}) Controller \end{itemize}Il numero di terminali del \textit{Boundary Scan} non varia con la complessità del sistema da testare, permettendo di collegarsi ad esso con un connettore standard JTAG. Il Boundary Scan può essere utilizzato - oltre che per i test - per l'emulazione di core e per l'\textit{in-circuit programming}.

\subsection{Boundary Scan Register}
\begin{figure} \centering
	\caption{Esempio di sistema con JTAG \textit{Boundary Scan}. Il \textit{Boundary Scan Register} è evidenziato dalle linee grigie}
	\label{fig:jtag}
	\includegraphics[height=0.66\textwidth,angle=90]{img/JTAG.pdf}
\end{figure}
Le \textit{boundary-scan cell} costituiscono uno \textit{shift-register} (figura~\ref{fig:jtag}): in questo modo è possibile avere soltanto due terminali (TDI e TDO) per comunicare in modo seriale con tutte le celle. Questo registro ha ingresso e uscita seriali per la comunicazione con l'esterno, mentre ha ingressi e uscite in parallelo per la connessione dei componenti del sistema. Il BSR può essere configurato in tre modalità\begin{itemize}
	\item \texttt{INTEST}: trasmissione di dati tra BSR e core, in questa configurazione il sistema di Boundary Scan può effettuare il test funzionale del core
	\item \texttt{EXTEST}: trasmissione di dati tra BSR ed il resto del sistema, in questa configurazione il sistema di Boundary Scan può funzionare come emulatore del core, sostituendosi ad esso
	\item \texttt{NOTEST}: trasmissione di dati tra core ed il resto del sistema, in questa configurazione non vengono effettuati test
\end{itemize}
Questi segnali arrivano dal TAP Controller a tutte le celle del Boundary Scan Register: ciascuno sarà il segnale di controllo di un corrispondente buffer three-state (tra la cella ed il core, tra la cella e l'esterno o tra il core e l'esterno). In un sistema composto da più componenti (come può essere un SoC), le catene di BSC dei vari componenti sono collegate in \textit{daisy chain} a formare un unico BSR.

\subsection{TAP Controller}
Il TAP Controller gestisce il funzionamento del Boundary Scan. Le istruzioni più comuni per il controller sono
\\\ \\{\renewcommand{\tabcolsep}{.2in} \begin{tabularx}{\textwidth}{lll}
		\textbf{Istruzione} & \textbf{Registro} & \textbf{Descrizione} \\\hline\hline
		\multirow{2}{*}{\texttt{EXTEST}}  & \multirow{2}{*}{BSR} & Test delle linee di connessione tra i \\ && dispositivi su board \\\hline
		\multirow{2}{*}{\texttt{BYPASS}} & \multirow{2}{*}{Bypass} & Connette TDI a TDO, saltando la catena \\ && di scan \\\hline 
		\multirow{2}{*}{\texttt{SAMPLE}} & \multirow{2}{*}{BSR} & Carica nel registro di scan i valori alle \\ && uscite del core \\\hline
		\multirow{2}{*}{\texttt{PRELOAD}} & \multirow{2}{*}{BSR} & Carica nel registro di scan i valori \\ && impostati tramite TDI \\\hline
		\multirow{2}{*}{\texttt{INTEST}} & \multirow{2}{*}{BSR} & \multirow{2}{*}{Applica un pattern agli ingressi del core} \\ && \\\hline
		\multirow{2}{*}{\texttt{IDCODE}} & \multirow{2}{*}{ID} & Legge l'identificativo memorizzato nel \\ && registro ID \\\hline
		\multirow{2}{*}{\texttt{USERCODE}} & \multirow{2}{*}{ID} & \multirow{2}{*}{Legge un identificativo opzionale} \\ &&\\\hline
		\multirow{2}{*}{\texttt{RUNBIST}} & \multirow{2}{*}{-} & \multirow{2}{*}{Attiva il BIST (\textit{Built-In Self-Test})} \\ &&\\\hline
		\multirow{2}{*}{\texttt{CLAMP}} & \multirow{2}{*}{Bypass} & Fissa i valori sulla scan chain e seleziona \\ && bypass \\\hline
		\multirow{2}{*}{\texttt{HIGHZ}} & \multirow{2}{*}{Bypass} & Porta i pin di uscita in alta impedenza \\ && e seleziona bypass 
		
	\end{tabularx} }
\chapter{Sintesi RTL}
La sintesi RTL (\textit{Register Transfer Level}) è un paradigma progettuale basato su un approccio \textit{datapath}/controllore che esprime la funzionalità del sistema non come tabella di verità, ma come trasferimenti di dati tra registri. I termini chiave della sintesi RTL sono \begin{itemize}
	\item Datapath \\ È il sottosistema che opera sui dati
	\item Controller \\ È il sottosistema che si occupa della generazione dei segnali di controllo
	\item Stato del sistema \\ È determinato dal contenuto dei registri
	\item Register transfer \\ È un'operazione svolta sui dati durante il trasferimento da un registro ad un altro. I register transfer implementano le funzionalità del sistema 
\end{itemize}
Il paradigma progettuale RTL è il seguente \begin{enumerate}
	\item Identificazione delle unità funzionali necessarie per le specifiche
	\item Identificazione delle operazioni e dei risultati intermedi per la progettazione dell'\textit{execution graph}
	\item Identificazione dell'algoritmo e delle strutture dati per la progettazione del \textit{datapath}
	\item Identificazione dei segnali di controllo necessari perché il \textit{datapath} rispetti l'\textit{execution graph}
	\item Progettazione del \textit{controller}
\end{enumerate}

\section{Moduli funzionali}
Una caratteristica dei progetti RTL è l'utilizzo di moduli funzionali al posto della progettazione basata esclusivamente su porte logiche. Una prima divisione dei moduli funzionali è tra circuiti combinatori e sequenziali: i circuiti combinatori dipendono solo dall'input, mentre i circuiti sequenziali dipendono anche dallo stato precedente del sistema. Sono circuiti combinatori i circuiti aritmetici (sommatori, moltiplicatori, ALU, \dots ), multiplexer/demultiplexer e encoder/decoder. Sono sequenziali i registri in tutte le loro varianti (paralleli/seriali, i contatori, le memorie, \dots ).

\paragraph{Timing Diagrams} I \textit{timing diagram} permettono di visualizzare l'andamento nel tempo dei segnali di I/O di un sistema. Essi permettono di visualizzare le relazioni tra i segnali e i tempi di attesa necessari per la lettura o la scrittura dei dati.

\subsection{Registri}
I registri sono unità funzionali di memorizzazione: un registro a N bit è composto da N \textit{flip-flop} tipo DT. Il flip-flop ha i terminali di uscita $Q$ sempre leggibili, mentre la scrittura del bit presente sul terminale $D$ avviene sul fronte di salita (o, a seconda del flip-flop, di discesa) del bit sul terminale $T$.

\subsubsection{Shift register}
\begin{figure} \centering
	\caption{Shift register a 4 bit con possibilità di scrittura e lettura sia in serie sia in parallelo}
	\label{fig:shiftreg}
	\includegraphics[width=\textwidth]{img/shiftreg.pdf}
\end{figure}
I registri a scorrimento sono banchi di flip-flop in serie caratterizzati dalla possibilità di fare scorrere i dati da un flip-flop al successivo. La scrittura può avvenire in parallelo (scrivendo tutti gli N bit negli N flip-flop) o in serie (facendo scorrere tutti i bit di una posizione e sovrascrivendo il bit nel primo flip-flop), la lettura può avvenire in parallelo (leggendo tutti gli N bit degli N flip-flop) o in serie (leggendo il bit dall'ultimo flip-flop). Alcuni circuiti hanno modalità di lettura e scrittura fisse (SISO/SIPO/PISO), mentre altri (figura~\ref{fig:shiftreg}) permettono di scegliere la modalità preferita.

\subsubsection{Contatore}
Un contatore è un modulo che conta gli impulsi in ingresso. È implementato con un banco di flip-flop \textit{toggle} in serie, con lettura in parallelo.

Un flip-flop toggle è un flip-flop DT con l'uscita $\overline{Q}$ collegata in retroazione in $D$: in questo modo quando arriva un impulso su $T$ lo stato del sistema viene invertito.

Per costruire un contatore a N bit, occorre collegare N flip-flop toggle in serie con l'uscita $Q$ di un flip-flop collegata al controllo $T$ del flip-flop successivo: in questo modo, quando un flip-flop passa da 1 a 0 manda un segnale di toggle al flip-flop successivo (come se fosse il riporto).

\subsection{Memorie}
Le memorie sono circuiti utilizzati per salvare dati. Il numero di bit di una parola di una memoria è detto larghezza della memoria (\textit{width}) e il numero di parole in una memoria è detto profondità (\textit{depth}).

\subsubsection{ROM}
\begin{figure} \centering
	\caption{Circuito di una ROM: in verde i conduttori con tensione \texttt{\apex{1}}}
	\label{fig:rom}
	\includegraphics[height=0.66\textwidth,angle=-90]{img/rom.pdf}
\end{figure}
Una memoria ROM (\textit{Read-Only Memory}) è una memoria non-riscrivibile. Il circuito in figura~\ref{fig:rom} rappresenta una ROM di larghezza $m$ e profondità $2^n$. Il decoder pone \texttt{\apex{0}} su tutti i conduttori $R$, eccetto il conduttore corrispondente al numero codificato dagli $n$ bit in ingresso (che codificano l'indirizzo della cella di memoria) che è posto a \texttt{\apex{1}}.

Sui conduttori $C$ viene imposta la tensione \texttt{\apex{1}} da una pre-carica temporanea (in figura, sul conduttore con l'etichetta $V_{DC}$): quando la tensione arriva, attiva il mosfet e si trasmette ai conduttori $C$, dopodiché i conduttori $C$ sono isolati tra loro dalla disattivazione del mosfet. Inizialmente, ognuno degli $2^n\cdot m$ incroci tra un conduttore $R$ e un conduttore $C$ è effettuato con un mosfet: il gate è collegato al conduttore $R$, il drain a massa e il source al conduttore $C$. Tutti gli indirizzi inattivi avranno i mosfet inattivi, per cui non influenzeranno il valore di tensione sul conduttore $C$. Per quanto riguarda l'indirizzo attivo $i$, il suo conduttore $R_i$ sarà posto a tensione \texttt{\apex{1}} dal decoder, per cui avrà i mosfet attivi: questi collegano i conduttori $C$ a massa, imponendovi la tensione \texttt{\apex{0}}. Eliminando il mosfet j-esimo della riga, il conduttore $C_j$ non sarà collegato a massa, mantenendo la pre-carica \texttt{\apex{1}}. La parola relativa alla cella i-esima della memoria è data dalla sequenza di \texttt{\apex{0}} e \texttt{\apex{1}} formata rispettivamente dalle presenze o assenze dei mosfet sul conduttore $R_i$.

Il procedimento di scrittura consiste, quindi, nel \quoted{bruciare} i mosfet relativi agli \texttt{\apex{1}} delle parole interessate. È un processo irreversibile (per questo è una memoria di sola lettura), ma il dato memorizzato si mantiene anche in assenza di corrente.

\subsection{SRAM}
\begin{figure} \centering
	\caption{Circuito di una SRAM}
	\label{fig:sram}
	\includegraphics[width=0.66\textwidth]{img/SRAM.pdf}
\end{figure}
Una RAM statica (figura~\ref{fig:sram}) memorizza le informazioni in una matrice di coppie di inverter. Quando una parola viene richiesta, i MOS vengono attivati dalla tensione \bittt{1} sul filo \textit{Word Line} e restituiscono sul filo $b_i$ il valore del bit richiesto (o, analogamente, ne abilitano la scrittura). Le SRAM non mantengono le informazioni quando viene tolta la corrente.

\section{Datapath}
Il datapath di un sistema può essere progettato ponendo le operazioni in sequenza o in concorrenza: i datapath concorrenti sono più performanti, mentre quelli sequenziali sono più economici perché permettono di sfruttare le stesse risorse per operazioni diverse.

Un modo per specificare operativamente un sistema è di definirne l'\textit{executive graph}: un grafo orientato tale per cui un parent deve essere eseguito prima di un figlio. I nodi alla stessa profondità sono concorrenti.

\section{Controllore}
Il \textit{controller} è l'architettura che gestisce la generazione dei segnali di controllo del sistema. Esso può essere centralizzato, se si tratta di un unico sottosistema che si occupa di controllare tutti i register transfer. Al contrario, è distribuito se il compito di generare i segnali di controllo è delegato ai moduli funzionali. È possibile, alternativamente, optare per soluzioni ibride (parzialmente centralizzata) in cui la maggior parte dei segnali sono generati da un controller, mentre altri sono generati da moduli funzionali.

\section{Register Transfer Notation}
Per specificare un sistema progettato con paradigma RTL è possibile utilizzare la \textit{Register Transfer Notation} (RTN, anche \textit{Register Transfer Language}). L'assegnamento (il trasferimento) è indicato con l'operatore \texttt{<-} e l'accesso a indici di memorie o registri con \texttt{[]}. L'esempio riporta la definizione dell'istruzione assembly \texttt{MOVE M[R1], R2}
\begin{verbatim}
// Fetch
MAR <- PC      // Memory-Address Register, Program Counter
MDR <- M[MAR]  // Memory-Data Register
IR  <- MDR     // Instruction Register

// Decode 

// Execute 
MAR <- R1
MDR <- R2
(MAR) <- MDR 

// Next
TR <- IR[n..m] // Jump
PC <- PC + TR
\end{verbatim}
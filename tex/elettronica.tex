\chapter{Elementi di Elettronica}

\begin{center}\begin{tabular}{|c|c|c|c|c|} \hline 
	Grandezza & Simbolo & Unità di Misura & Simbolo & Dimensione \\\hline\hline 
	Intensità di corrente & I & ampere & A & $[\ I\ ]$ \\\hline 
	Quantità di carica & Q & coulomb & C & $[\ I\ ] [\ T\ ]$ \\\hline 
	Potenziale & \multirow{2}{*}{V} & \multirow{2}{*}{volt} & \multirow{2}{*}{V} & $ [\ M\ ] [\ L\ ]^2 $ \\\cline{5-5}
	elettrico & & & & $[\ T\ ]^3 [\ I\ ]$ \\\hline 
\end{tabular}\end{center}
Nei materiali conduttori gli elettroni possono passare liberamente fra gli atomi, quindi, per repulsione, si dispongono gli uni equidistanti dagli altri, quindi il potenziale elettrico è uniforme nel corpo. Nei materiali isolanti, le cariche rimangono confinate alla zona in cui sono.

\paragraph{Leggi di Kirchoff}
Secondo l'\textit{equazione alle maglie}, la somma delle tensioni lungo una maglia è nulla. Secondo l'\textit{equazione ai nodi}, la somma delle correnti entranti in un nodo è nulla.
$$ \sum_{i} V_i = 0 \hspace{2in} \sum_{i} I_i = 0 $$

\section{Bipoli equivalenti}
I \textit{bipoli} sono dispositivi (o insiemi di dispositivi) caratterizzati da due terminali (i \textit{poli}) e una differenza di potenziale. Possono essere semplificati dai bipoli equivalenti (figura~\ref{fig:norton}).
\begin{figure} \centering
	\caption{Bipoli equivalenti di Thevenin e di Norton}
	\label{fig:norton}
	\ \newline 
	\includegraphics[width=0.4\textwidth]{img/equivalents.pdf}
\end{figure}

\paragraph{Bipolo equivalente di Norton}
Il bipolo equivalente di Norton di un circuito è composto da un resistore e da un generatore di corrente disposti in parallelo.

\paragraph{Bipolo equivalente di Thevenin}
Il bipolo equivalente di Thevenin di un circuito è composto da un resistore e da un generatore di tensione disposti in serie.

\section{Bipoli lineari}
Oltre ai generatori, le reti lineari sono composte dai bipoli lineari RCL (figura~\ref{fig:rcl}). Sui sistemi lineari vale il principio di sovrapposizione degli effetti.
$$ E\left( \sum cause \right) = \sum E \left( cause \right) $$
Le relazioni che instaurano fra tensione e intensità di corrente sono
\begin{center}\begin{tabular}{|rl||c|c|}\hline 
	R & Resistore & $V(t)=R\cdot I(t)$ & $I(t)=\nicefrac{1}{R}\cdot V(t)$ \\\hline 
	C & Condensatore & $V(t)=\nicefrac{1}{C} \int_{-\infty}^{t}I(\tau)\ d\tau$ & $I(t)=C\ \nicefrac{dV}{dt}(t)$ \\\hline 
	L & Induttore & $V(t)=L\ \nicefrac{dI}{dt}(t)$ & $I(t)=\nicefrac{1}{L} \int_{-\infty}^{t}V(\tau)\ d\tau$ \\\hline 
\end{tabular}\end{center}
\begin{figure} \centering
\caption{Bipoli lineari: resistori, condensatori, induttori.}
\label{fig:rcl}
\ \newline 
\includegraphics[width=0.66\textwidth]{img/RCL.pdf}
\end{figure}
\subsection{Resistori}
I resistori, o resistenze, si oppongono al passaggio di corrente secondo la legge di Ohm
$$ I = \frac{V}{R} $$
La costante della proporzionalità diretta tra tensione e intensità di corrente è la resistenza $R$ (o conduttanza $\nicefrac{1}{R}$). L'unità di misura della resistenza è l'ohm ($\Omega$).

\paragraph{Resistenze in Serie}
Dei resistori collegati in serie (figura~\ref{fig:spres}) sono equivalenti ad un resistore che abbia un valore di resistenza pari alla somma delle resistenze.

Per l'equazione ai nodi, tutta la corrente che passa in un resistore passa anche nel successivo. Quindi, per ogni resistore $R_i$ vale 
$$ V_i = R_i\cdot I $$
La caduta di tensione totale è uguale alla somma delle cadute di tensione
$$ V_{tot} = \sum_{i} R_i\cdot I $$
Da cui, la resistenza totale delle resistenze in serie (\textit{resistenza equivalente}) è 
$$ R_{eq} = \frac{ \sum_{i} R_i\cdot I }{I} = \sum_{i} R_i $$

\subparagraph{Partitore Resistivo}
\begin{figure} \centering
	\caption{Resistenze in serie e in parallelo. Partitore resistivo}
	\label{fig:spres}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/spres.pdf}
	\\ 
	\includegraphics[width=0.27\textwidth]{img/partres.pdf}
\end{figure}
Il partitore resistivo (figura~\ref{fig:spres}) è un sistema formato da due resistenze in serie: la tensione in ingresso è applicata ai capi del sistema, mentre la tensione in uscita è rilevata ai capi di $R_2$.

La tensione in uscita è 
$$V_{out}=R_2\cdot I= \frac{R_2}{R_1+R_2}\cdot V_{in}$$
Possiamo ottenere una frazione a piacere della tensione in ingresso scegliendo i valori delle resistenze.

\paragraph{Resistenze in Parallelo}
Dei resistori collegati in parallelo (figura~\ref{fig:spres}) sono equivalenti ad un resistore che abbia un valore di conduttanza pari alla somma delle conduttanze.

La differenza di potenziale ai capi dei resistori è uguale per tutti, perché sono collegate ai capi
$$ V = R_i\cdot I_i $$
Per l'equazione ai nodi, l'intensità di corrente uscente dal sistema è uguale alla somma delle intensità di corrente che attraversano i resistori
$$ I_{tot} = \sum_{i} I_i = \sum_{i} \frac{V}{R_i} = V\cdot \sum_{i} \frac{1}{R_i} $$
Da cui, la resistenza equivalente delle resistenze in parallelo è 
$$ R_{eq} = \frac{ V }{V\cdot \sum_{i} \frac{1}{R_i}} = \frac{ 1 }{ \sum_{i} \frac{1}{R_i} }\hspace{1in} \frac{1}{R_{eq}} = \sum_{i} \frac{1}{R_i}$$

\subsection{Condensatori}
\begin{figure} \centering
	\caption{Condensatori in serie e in parallelo}
	\label{fig:spcond}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/spcond.pdf}
\end{figure}
I condensatori sono formati da una coppia di armature in cui la carica si accumula, ma tra cui non passa corrente. La capacità $C$ del condensatore determina la quantità di carica immagazzinata e si misura in farad (F).
$$C=\frac{Q}{V}$$
La proporzionalità tra tensione e intensità è 
$$V(t)=\frac{Q(t)}{C}=\frac{1}{C} \int_{-\infty}^{t}I(\tau)\ d\tau $$
$$I(t)=C\ \frac{dV}{dt}(t)$$

\paragraph{Condensatori in Serie}
Dei condensatori collegati in serie (figura~\ref{fig:spcond}) sono equivalenti ad un condensatore che abbia un valore di capacità il cui reciproco sia pari alla somma dei reciproci delle capacità.

Dato che i condensatori sono collegati in serie con un conduttore, la carica sull'armatura destra di un condensatore sarà uguale e opposta alla carica sull'armatura sinistra del condensatore seguente. Per ogni condensatore 
$$ Q = C_i\cdot V_i $$
La caduta di tensione totale è uguale alla somma delle cadute di tensione
$$ V_{tot} = \sum_{i} \frac{Q}{C_i} = Q\cdot \sum_{i} \frac{1}{C_i} $$
Da cui, la capacità totale dei condensatori in serie (\textit{capacità equivalente}) è 
$$ C_{eq} = \frac{ Q }{ Q\cdot \sum_{i} \frac{1}{C_i}  } = \frac{1}{ \sum_{i} \frac{1}{C_i} }\hspace{1in} \frac{1}{C_{eq}}=\sum_{i} \frac{1}{C_i} $$

\paragraph{Condensatori in Parallelo}
Dei condensatori collegati in parallelo (figura~\ref{fig:spcond}) sono equivalenti ad un condensatore che abbia un valore di capacità pari alla somma delle capacità.

La differenza di potenziale ai capi dei condensatori è uguale per tutti, perché sono collegati ai capi
$$ Q_i = C_i\cdot V $$
Il condensatore equivalente immagazzinerà una quantità di carica pari alla somma delle quantità di carica immagazzinate
$$ Q_{tot} = \sum_{i} C_i\cdot V $$
Da cui, la capacità equivalente dei condensatori in parallelo è 
$$ C_{eq} = \frac{ \sum_{i} C_i\cdot V }{ V } = \sum_{i} C_i $$

\subsection{Induttori}
Gli induttori sono conduttori a spirale (o toroide, \dots ), nei quali il passaggio di corrente induce un campo magnetico, che a sua volta induce una differenza di potenziale che si oppone ai cambiamenti di intensità di corrente.

\begin{figure} \centering
	\caption{Diodi: diodo generico, led. Funzione caratteristica ideale e reale.}
	\label{fig:diodes}
	\ \newline 
	\includegraphics[width=0.4\textwidth]{img/LEDs.pdf}
	\\\ \\
	\includegraphics[width=1\textwidth]{img/dioderesp.pdf}
\end{figure}
La legge di Faraday lega tensione e flusso di campo magnetico
$$ V(t)=-\frac{d\Phi_B}{dt}(t) $$
Il flusso di campo magnetico attraverso la sezione dell'induttore è 
$$\Phi_B=\int_{s} \overrightarrow{B}\ ds = \left| \overrightarrow{B} \right|\cdot S$$
Il modulo del campo magnetico indotto è
$$ \left| \overrightarrow{B}(t) \right| = \mu \frac{N}{l} I(t) $$
dove $\mu$ è la costante di permeabilità magnetica, $N$ è il numero di spire e $l$ è la lunghezza dell'induttore.
$$ V(t)= -\frac{d\Phi_B}{dt} = -\frac{d\left\lbrace \left| \overrightarrow{B}(t) \right|\cdot S \right\rbrace}{dt} = -\frac{d\left\lbrace \mu \frac{N}{l} I(t)\cdot S \right\rbrace}{dt}(t) = -\mu \frac{N}{l} S \ \frac{dI}{dt}(t)  = L \frac{dI}{dt}(t) $$
$$ I(t)=\frac{1}{L} \int_{-\infty}^{t}V(\tau)\ d\tau  $$

\section{Bipoli non lineari}
\subsection{Diodi}
Un diodo (figura~\ref{fig:diodes}) consente il passaggio di corrente solo in una direzione, dall'anodo verso il catodo. Alcuni diodi emetto luce al passaggio della corrente: sono i LED (\textit{light emitting diode}).

A causa della risposta esponenziale ad un aumento di tensione, i diodi non andrebbero pilotati in tensione (un piccolo errore sulla tensione produrrebbe un grande errore sull'intensità), ma in intensità.

\subsection{Amplificatore Operazionale}
\begin{figure} \centering
	\caption{Amplificatore operazionale}
	\label{fig:opamp}
	\ \newline 
	\includegraphics[width=0.25\textwidth]{img/opamp.pdf}
\end{figure}
Un amplificatore operazionale (\textit{op-amp}) è un circuito integrato che ha due terminali in ingresso, un terminale invertente $V_-$ e un terminale non-invertente $V_+$ e un terminale in uscita su cui viene riportata una tensione che è l'amplificazione della differenza delle tensioni in input (figura~\ref{fig:opamp})
$$V_{out}=G\cdot (V_+-V_-)$$
Gli amplificatori operazionali sono caratterizzati da guadagni molto alti, resistenze di ingresso elevatissime e assorbimento di corrente bassissimo
$$G \sim 10^6 \hspace{.66in} R_{in} \sim 10^6~\Omega \hspace{.66in} R_{out} \sim 50~\Omega \hspace{.66in} I_{in} < 10^{-12}~A $$
Dato che i guadagni sono così elevati, utilizzato così com'è non è utilizzabile come amplificatore: infatti, se alimentato, per esempio, da una batteria a 9V, anche una differenza di potenziale molto piccola $\Delta V \sim 10^{-5}$ manderebbe l'amplificatore in saturazione.

\paragraph{Retroazione negativa}
Per questo, si utilizza sfruttando la retroazione negativa: l'output viene collegato in \textit{feedback} al terminale invertente. In questa configurazione la differenza di potenziale tende a 0, perché \begin{itemize}
	\item se $\Delta V$ fosse positiva, sul terminale invertente verrebbe riportata la tensione massima della batteria, quindi $\Delta V$ diventerebbe negativa 
	\item se $\Delta V$ fosse negativa, sul terminale invertente verrebbe riportata la tensione minima della batteria, quindi $\Delta V$ diventerebbe positiva 
\end{itemize}

\subsubsection{Amplificatore invertente}
\begin{figure} \centering
	\caption{Amplificatore invertente}
	\label{fig:invertamp}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/invertamp.pdf}
\end{figure}
L'amplificatore invertente (figura~\ref{fig:invertamp}) è un amplificatore operazionale con il terminale non-invertente collegato a massa, un resistore $R_1$ in ingresso e un resistore $R_2$ sul collegamento in retroazione.

A causa di quanto detto sulla retroazione negativa, la tensione sul filo collegato al terminale invertente deve essere uguale a quella sul terminale non-invertente, che è a massa: ne consegue che quel filo è alla tensione della massa, senza esservi collegato, è a \textit{massa virtuale}.

Dato il - quasi - nullo assorbimento di energia da parte del terminale dell'op-amp, tutta l'intensità di corrente $I$ in ingresso su $R_1$ viene reindirizzata su $R_2$. Essa è, quindi
$$ I = \frac{V_{in}-0}{R_1} = \frac{V_{in}}{R_1} \hspace{1in} I = \frac{0-V_{out}}{R_2} = -\frac{V_{out}}{R_2} $$
Da cui abbiamo che la tensione in uscita è
$$V_{out} = -\frac{R_2}{R_1}\cdot V_{in}$$
Abbiamo ottenuto un amplificatore con guadagno regolabile (in funzione delle due resistenze), che inverte il segno della tensione in ingresso. La resistenza in ingresso è $R_1$, la resistenza in uscita è quella dell'op-amp.

\subsubsection{Amplificatore non-invertente}
\begin{figure} \centering
	\caption{Amplificatore non-invertente. Buffer}
	\label{fig:ninvertamp}
	\ \newline 
	\includegraphics[height=0.18\textheight]{img/ninvertamp.pdf}
	\includegraphics[height=0.1735\textheight]{img/buffer.pdf}
\end{figure}
L'amplificatore non-invertente (figura~\ref{fig:ninvertamp}) è un amplificatore operazionale con il terminale invertente collegato a massa tramite un resistore $R_1$, un resistore $R_2$ è sul collegamento in retroazione e l'ingresso è collegato al terminale non-invertente.

A causa di quanto detto sulla retroazione negativa, la tensione sul filo collegato al terminale invertente deve essere uguale a quella sul terminale non-invertente, che è $V_{in}$.

Dato il - quasi - nullo assorbimento di energia da parte del terminale dell'op-amp, tutta l'intensità di corrente $I$ che passa su $R_1$ viene reindirizzata su $R_2$. Essa è, quindi
$$ I = \frac{0-V_{in}}{R_1} = -\frac{V_{in}}{R_1} \hspace{1in} I = \frac{V_{in}-V_{out}}{R_2} $$
Da cui abbiamo che la tensione in uscita è
$$ V_{out} = \frac{(R_1+R_2)}{R_1}\cdot V_{in} = \left(1+\frac{R_2}{R_1}\right)\cdot V_{in} $$
Abbiamo ottenuto un amplificatore con guadagno regolabile (in funzione delle due resistenze), che non inverte il segno della tensione in ingresso, ma non può attenuare. Le resistenze in ingresso e in uscita sono quelle dell'op-amp.

\paragraph{Buffer} Un buffer è un amplificatore non-invertente con guadagno unitario: si ottiene eliminando il filo con $R_1$ ($R_1 \rightarrow +\infty$) e eliminando la resistenza $R_2$ ($R_2 \rightarrow 0$). Il guadagno che ne risulta è, appunto
$$ G=\left(1+\frac{R_2}{R_1}\right)=\left(1+\frac{0}{+\infty}\right)=1 $$
Il buffer consente di mantenere la tensione originaria, pur rafforzando il segnale abbassando la resistenza da una resistenza d'ingresso $R_{op_{in}}\sim 10^6\ \Omega$ a una resistenza d'uscita $R_{op_{out}}\sim 50\ \Omega$.

\subsubsection{Sommatore invertente}
\begin{figure} \centering
	\caption{Somatore invertente}
	\label{fig:invsum}
	\ \newline 
	\includegraphics[width=0.66\textwidth]{img/invsum.pdf}
\end{figure}
Il sommatore invertente (figura~\ref{fig:invsum}) è un amplificatore invertente al cui input convergono $n$ resistori. La corrente in ingresso, uguale a quella in uscita per quanto detto sull'amplificatore invertente, è la somma delle correnti passanti sui resistori
$$I = \sum_{i}\frac{V_i}{R_i}\hspace{1in} I = -\frac{V_{out}}{R}$$
La tensione in uscita è, quindi, l'opposto della somma delle tensioni in ingresso pesata secondo le conduttanze dei conduttori di ingresso
$$ V_{out} = -R\cdot \sum_{i}\frac{V_i}{R_i} $$

\subsubsection{Amplificatore differenziale}
\begin{figure} \centering
	\caption{Amplificatore differenziale}
	\label{fig:diffamp}
	\ \newline 
	\includegraphics[width=0.8\textwidth]{img/diffamp.pdf}
\end{figure}
Sfruttiamo il principio di sovrapposizione degli effetti e consideriamo gli effetti delle due tensioni singolarmente.

Non consideriamo $V_2$ ($V_2=0$). Il circuito risultante è un amplificatore invertente, quindi $$V_{out_1}=-\frac{R_F}{R_G}\cdot V_1$$
Non consideriamo $V_1$ ($V_1=0$). Il circuito risultante è un amplificatore non-invertente: l'ingresso dell'op-amp è l'uscita del partitore resistivo formato da $R_G$ e $R_F$. Quindi l'uscita dell'op-amp è
$$V_{out_2}=\frac{R_F+R_G}{R_G}\cdot V_+=\frac{R_F+R_G}{R_G}\cdot \frac{R_F}{R_F+R_G}\cdot V_2=\frac{R_F}{R_G}\cdot V_2$$
Per il principio di sovrapposizione degli effetti, l'uscita totale del circuito è 
$$V_{out}=V_{out_1}+V_{out_2}=-\frac{R_F}{R_G}\cdot V_1+\frac{R_F}{R_G}\cdot V_2=\frac{R_F}{R_G}\cdot \left(V_2 - V_1\right)$$
Ovvero, l'uscita è un'amplificazione di guadagno $\nicefrac{R_F}{R_G}$ della differenza dei due potenziali in ingresso.
La resistenza in ingresso è $R_G$ e la resistenza in uscita è quella dell'op-amp.

\paragraph{Amplificatore per strumentazione} L'amplificatore per strumentazione (figura~\ref{fig:instramp}) è un amplificatore amplificatore differenziale usato per ridurre il rumore additivo che degrada i segnali: su un conduttore viene fatto passare il segnale e su un conduttore molto vicino - quindi, soggetto allo stesso rumore - un segnale vuoto, dopodiché il segnale viene ottenuto come differenza tra i due segnali degradati $V(t)= \left[ V(t)+N(t) \right]-\left[ N(t) \right]$.

\begin{figure} \centering
	\caption{Amplificatore per strumentazione}
	\label{fig:instramp}
	\ \newline 
	\includegraphics[width=0.8\textwidth]{img/instramp.pdf}
\end{figure}
Per quanto detto sulla retroazione negativa, sui terminali negativi dei due buffer in ingresso sono riportate le tensioni $V_1$ e $V_2$. L'intensità di corrente che attraversa $R_G$ è 
$$I=\frac{V_1-V_2}{R_G}$$
Dato il bassissimo assorbimanto di corrente degli op-amp, tutta l'intensità di corrente attraversa anche i due resistori $R_1$, oltre che $R_G$. Per il principio delle resistenze in serie, la differenza di potenziale ai capi del sistema di resistenze $R_1-R_G-R_1$ è 
$$\Delta V = (2R_1+R_G)\cdot I  = (2R_1+R_G)\cdot \frac{V_1-V_2}{R_G} = \left(1+2\frac{R_1}{R_G}\right)\cdot (V_1-V_2) $$
Il sistema formato dai due resistori $R_2$, i due resistori $R_3$ e l'op-amp è un amplificatore differenziale, quindi
$$ V_{out} = \frac{R_3}{R_2}\cdot \Delta V = \frac{R_3}{R_2}\left(1+2\frac{R_1}{R_G}\right)\cdot (V_1-V_2) $$
Il guadagno è $\nicefrac{R_3}{R_2}\left(1+2\ \nicefrac{R_1}{R_G}\right)$. La principale differenza rispetto ad un semplice amplificatore differenziale è la resistenza di ingresso, che è la resistenza di ingresso dell'op-amp.

\subsubsection{Integratore}
\begin{figure} \centering
	\caption{Integratore, derivatore}
	\label{fig:integrator}
	\label{fig:deriver}
	\ \newline 
	\includegraphics[width=0.45\textwidth]{img/integrator.pdf}
	\includegraphics[width=0.45\textwidth]{img/deriver.pdf}
\end{figure}
L'integratore (figura~\ref{fig:integrator}) svolge l'operazione matematica dell'integrazione. Per quanto detto sulla retroazione negativa, $V_-$ è uguale a $V_+$, quindi è a massa virtuale. L'intensità di corrente che attraversa il resistore è $$I(t)=\frac{V_{in}(t)}{R}$$
Per quando riguarda il condensatore, il legame tra tensione e intensità è
$$ V_{out}(t)=-\frac{1}{C}\int_{0}^{t}I(\tau)\ d\tau $$
Da cui 
$$ V_{out}(t)=-\frac{1}{RC}\int_{0}^{t}V_{in}(\tau)\ d\tau $$
Si noti che la costante del sistema $RC$ ha le dimensione di un tempo
$$ \Omega\cdot F = \frac{V}{A}\cdot \frac{C}{V} = \frac{C}{A} = s $$
Sarà  $T:=RC$ la costante di tempo del circuito integratore
$$ V_{out}(t)=-\frac{1}{T}\int_{0}^{t}V_{in}(\tau)\ d\tau $$
Al tempo $t=T$ la tensione sarà l'opposto della tensione media rilevata nell'intervallo di tempo $t=[0,T]$
$$ V_{out}(T)=-\frac{1}{T}\int_{0}^{T}V_{in}(\tau)\ d\tau $$

\subsubsection{Derivatore}
Il derivatore (figura~\ref{fig:deriver}) è un circuito che svolge l'operazione matematica di derivazione. Per quanto detto sulla retroazione negativa, $V_-$ è uguale a $V_+$, quindi è a massa virtuale. L'intensità di corrente che attraversa il resistore è $$I(t)=\frac{-V_{out}(t)}{R}$$
Per quando riguarda il condensatore, il legame tra intensità e tensione è
$$ I(t)=C\cdot \frac{dV_{in}}{dt}(t) $$
Da cui 
$$ V_{out}(t)=-RC\cdot \frac{dV_{in}}{dt} =-T\cdot \frac{dV_{in}}{dt} $$

\subsubsection{Amplificatore logaritmico}
\begin{figure} \centering
	\caption{Amplificatore logaritmico, raddrizzatore}
	\label{fig:logamp}
	\label{fig:recto}
	\ \newline 
	\includegraphics[width=0.45\textwidth]{img/logamp.pdf}
	\includegraphics[width=0.45\textwidth]{img/recto.pdf}
\end{figure}
L'amplificatore logaritmico (figura~\ref{fig:logamp}) è un circuito che svolge l'operazione matematica di logaritmo. Per quanto detto sulla retroazione negativa, $V_-$ è uguale a $V_+$, quindi è a massa virtuale. L'intensità di corrente che attraversa il resistore è $$I(t)=\frac{V_{in}(t)}{R}$$
Per quando riguarda il diodo, il legame tra intensità e tensione è
$$ I(t) \approx I_s\cdot \exp\left( -\frac{V_{out}(t)}{V_{th}} \right) $$
Dove $V_{th}$ è la tensione di soglia del diodo e $I_s$ è la corrente di saturazione. Da qui abbiamo che
$$ V_{out}(t) = -V_{th}\cdot \ln\left(\frac{I(t)}{I_s}\right) = -V_{th}\cdot \ln\left(\frac{V_{in}(t)}{R\cdot I_s}\right) $$

\subsubsection{Raddrizzatore}
Il raddrizzatore (figura~\ref{fig:recto}) è un circuito che svolge l'operazione matematica di gradino unitario. La tensione in uscita dall'op-amp è 
$$ V_A = A\cdot (V_{in}-V_{out}) $$
\paragraph{Caso A}
Per valori di tensione $V_A \geq 0$
$$ V_A = V_{out}+\Delta V_D $$
Quindi 
$$ V_{in} = \frac{V_{out}+\Delta V_D}{A}+V_{out} = \frac{(A+1)\cdot V_{out}+\Delta V_D}{A} $$
Dato che $A \sim 10^{12}$ possiamo approssimare con una minorazione
$$ V_{in} \approx V_{out} \hspace{1in} ( V_{in} \geq V_{out} ) $$
\paragraph{Caso B}
Per valori di tensione $V_A \leq 0$ 
$$V_{out} = 0$$
Perché il diodo impedisce il passaggio di corrente. 
Abbiamo, quindi
\begin{equation} \nonumber 
V_{out} \approx 
\begin{cases*}
V_{in} & $ V_{in} \geq 0 $ \\
0 & $ V_{in} < 0 $ \\
\end{cases*}
\end{equation}

\section{Elettronica Digitale}
In elettronica digitale si hanno due soli valori di tensione: la tensione minima \texttt{\apex{0}} e la tensione massima \texttt{\apex{1}}. Ogni conduttore porta 1 bit di informazione.
\subsection{MOSFET}
\begin{figure} \centering
	\caption{Costituzione di un MOSFET (tipo N). Invertitore implementato con tecnologia CMOS (tipo N a sinistra, tipo P a destra)}
	\label{fig:mosfet}
	\ \\
	\includegraphics[width=0.45\textwidth]{img/mosfet.pdf}
	\includegraphics[width=0.45\textwidth]{img/cmosinverter.pdf}
\end{figure}
I MOSFET (\textit{Metal-Oxide-Semiconductor Field-Emitting Transistor}) sono componenti formati da conduttori al silicio, caricati positivamente con aggiunta di alluminio o negativamente con fosforo, uno strato isolante di ossido (\textit{gate oxide}) e uno strato conduttore (\textit{gate}). I MOSFET di tipo P hanno i due terminali \textit{drain} e \textit{source} carichi positivamente ed il sostrato negativamente, mentre i MOSFET di tipo N (figura~\ref{fig:mosfet}) hanno i terminali negativi e il sostrato positivo.

Un innalzamento di tensione sul gate di un MOSFET di tipo N provoca un accumulo di cariche positive sul terminale: ciò attrae le cariche negative sui terminali e, oltre una tensione di soglia $V_{th}$, permette la conduzione degli elettroni tra drain e source. Al contrario, una tensione bassa non permette la conduzione degli elettroni tra i terminali. Quindi, un MOSFET di tipo N si comporta come un collegamento aperto se sul gate c'è \texttt{\apex{0}} o come un collegamento chiuso se sul gate c'è \texttt{\apex{1}}. Inversamente, un MOSFET di tipo P si comporta come un collegamento chiuso se sul gate c'è \texttt{\apex{0}} o come un collegamento aperto se sul gate c'è \texttt{\apex{1}}. 

\subsubsection{CMOS}
La tecnologia CMOS (\textit{Complementary MOSFET}) utilizza MOSFET complementari (tipo N e tipo P) per implementare circuiti logici. Un esempio è l'\textit{inverter} CMOS (figura~\ref{fig:mosfet}). In caso $V_{in}$ sia \texttt{\apex{0}}, il MOSFET di tipo N è aperto, mentre quello di tipo P è chiuso e fa passare la tensione \texttt{\apex{1}}. Al contrario, se $V_{in}$ fosse \texttt{\apex{1}}, il MOSFET di tipo P sarebbe aperto, mentre quello di tipo N sarebbe chiuso e farebbe passare la tensione \texttt{\apex{0}}.

\subsection{Accorgimenti tecnici}
\begin{figure} \centering
	\caption{Rise time e fall time}
	\label{fig:risefalltime}
	\ \\
	\includegraphics[width=0.66\textwidth]{img/risefalltime.pdf}
\end{figure}
\begin{figure} \centering
	\caption{Data race, static hazard causato dal ritardo introdotto dall'inverter}
	\label{fig:datarace}
	\ \\
	\includegraphics[width=0.25\textwidth]{img/dataracecircuit.pdf}\\
	\includegraphics[width=.9\textwidth]{img/datarace.pdf}
\end{figure}
\subsubsection{Porte three-state}
Nel caso in cui collegassimo le uscite di due porte logiche portemmo avere un corto circuito. Per evitare che ciò avvenga esistono le porte \textit{three-state}: esse hanno, appunto, tre stati
\begin{center}\renewcommand{\tabcolsep}{.3cm}\begin{tabular}{cccccc}
	\texttt{\apex{0}} & Bassa tensione &
	\texttt{\apex{1}} & Alta tensione &
	\texttt{\apex{Z}} & Alta impedenza 
\end{tabular}\end{center}
Le porte, oltre agli ingressi logici, avranno un ingresso di attivazione: se la porta è attiva riporta in uscita il risultato dell'operazione logica, se è disattivata si trova nello stato di alta impedenza, nel quale è come se la porta fosse scollegata. È possibile, così, collegare le uscite di diverse porte senza rischio di corto circuito se le attiviamo in mutua esclusione.

\subsubsection{Fan in/out}
Il \textit{fan out} è la misura del numero massimo di ingressi collegabili ad un uscita
$$fan\ out = \min\left( \left\lfloor \frac{I_{out_{max}}}{I_{in_{max}}} \right\rfloor , \left\lfloor \frac{I_{out_{min}}}{I_{in_{min}}} \right\rfloor \right)$$
Per i CMOS il fan out è di circa $26000$, però più porte si collegano più rallentano le transizioni.
Il \textit{fan in} è l'intensità di corrente minima per pilotare un ingresso.

\subsubsection{Rise/fall time}
Il tempo di ascesa (\textit{rise time}) è il tempo impiegato da un circuito per passare dallo stato di bassa tensione (da $V \in [V_0,\ (V_1-V_0)\cdot 0.10+V_0]$) allo stato di alta tensione (a $V \in [(V_1-V_0)\cdot 0.90+V_0,\ V_1]$). Il tempo di discesa (\textit{fall time}) è il tempo impiegato da un circuito per passare dallo stato di alta tensione allo stato di bassa tensione ed è maggiore del rise time (figura ~\ref{fig:risefalltime}).

\subsubsection{Tempo di commutazione}
Dato il tempo di propagazione del segnale su una porta $t_p$ e il numero massimo di porte attraversate dal segnale dall'ingresso all'uscita, detto \textit{cammino critico}, il tempo di commutazione del circuito è il ritardo $t_p\cdot |CamminoCritico|$ dopo il quale è sicuramente stabile il valore sull'uscita della porta. A causa dei tempi di propagazione, possono avvenire delle \textit{data races}, che possono produrre degli errori singoli di tipo \textit{static hazard} (figura~\ref{fig:datarace}) o, in presenza di più porte, di transizioni multiple (\textit{glitches}).

\section{Semplificazione di circuiti logici}
I circuiti logici possono essere semplificati con il metodo di Quine-McCluskey
\begin{enumerate}
	\item Accorpamento
	\begin{enumerate}
		\item Ordinamento degli implicanti per numero crescente di \texttt{\apex{1}}
		\item Riduzione degli implicanti accorpando gli implicanti a distanza di Hamming 1
	\end{enumerate}
	\item Copertura minima
	\begin{enumerate}
		\item Individuo le colonne essenziali
		\item Elimino le colonne dominate
		\item Elimino le righe dominanti
	\end{enumerate}
\end{enumerate}
Nel caso in cui ci siamo righe non definite nella tabella di verità, le considero per la fase 1 e, poi, le scarto.

\paragraph{Esempio} \begin{center} \begin{tabular}{c | cccc|c}
	 & x & y & z & v & o \\\hline 
	$m_{0}$ & 0 & 0 & 0 & 0 & 0 \\
	$m_{1}$ & 0 & 0 & 0 & 1 & 1 \\
	$m_{2}$ & 0 & 0 & 1 & 0 & 0 \\
	$m_{3}$ & 0 & 0 & 1 & 1 & 0 \\
	$m_{4}$ & 0 & 1 & 0 & 0 & 1 \\
	$m_{5}$ & 0 & 1 & 0 & 1 & 1 \\
	$m_{6}$ & 0 & 1 & 1 & 0 & 1 \\
	$m_{7}$ & 0 & 1 & 1 & 1 & 1 \\
	$m_{8}$ & 1 & 0 & 0 & 0 & 0 \\
	$m_{9}$ & 1 & 0 & 0 & 1 & 1 \\
	$m_{10}$ & 1 & 0 & 1 & 0 & 0 \\
	$m_{11}$ & 1 & 0 & 1 & 1 & 1 \\
	$m_{12}$ & 1 & 1 & 0 & 0 & 0 \\
	$m_{13}$ & 1 & 1 & 0 & 1 & 0 \\
	$m_{14}$ & 1 & 1 & 1 & 0 & 1 \\
	$m_{15}$ & 1 & 1 & 1 & 1 & 1 
\end{tabular} \end{center}
Ordinamento degli implicanti e accorpamento 
\begin{center} \renewcommand{\tabcolsep}{.13cm}\begin{tabular}{|l|llll|}\hline 
	& x & y & z & v \\\hline 
	$m_{1}$ & 0$^{b}$ & 0$^{a}$ & 0 & 1 \\
	$m_{4}$ & 0 & 1 & 0$^{d}$ & 0$^{c}$ \\\hline \hline
	$m_{5}$ & 0 & 1$^{a}$ & 0$^{e}$ & 1$^{c}$ \\
	$m_{6}$ & 0$^{g}$ & 1 & 1$^{d}$ & 0$^{f}$ \\
	$m_{9}$ & 1$^{b}$ & 0 & 0$^{h}$ & 1 \\\hline \hline 
	$m_{7}$ & 0$^{i}$ & 1 & 1$^{e}$ & 1$^{f}$ \\
	$m_{11}$ & 1 & 0$^{j}$ & 1$^{h}$ & 1 \\
	$m_{14}$ & 1$^{g}$ & 1 & 1 & 0$^{k}$ \\\hline \hline 
	$m_{15}$ & 1$^{i}$ & 1$^{j}$ & 1 & 1$^{k}$ \\\hline 
\end{tabular}
\begin{tabular}{|ll|llll|}\hline 
	& & x & y & z & v \\\hline 
	(A) & $m_{1,5}$ & 0 & \texttt{-} & 0 & 1 \\
	(B)	& $m_{1,9}$ & \texttt{-} & 0 & 0 & 1 \\
	& $m_{4,5}$ & 0 & 1 & 0$^a$ & \texttt{-} \\
	& $m_{4,6}$ & 0 & 1 & \texttt{-} & 0$^b$ \\\hline\hline 
	& $m_{5,7}$ & 0 & 1 & \texttt{-} & 1$^b$ \\
	& $m_{6,7}$ & 0$^c$ & 1 & 1$^a$ & \texttt{-} \\
	& $m_{6,14}$ & \texttt{-} & 1 & 1 & 0$^d$ \\
	(C)	& $m_{9,11}$ & 1 & 0 & \texttt{-} & 1 \\\hline \hline 
	& $m_{7,15}$ & \texttt{-} & 1 & 1 & 1$^d$ \\
	(D)	& $m_{11,15}$ & 1 & \texttt{-} & 1 & 1 \\
	& $m_{14,15}$ & 1$^c$ & 1 & 1 & \texttt{-} \\\hline 
\end{tabular}
\begin{tabular}{|ll|llll|}\hline 
& & x & y & z & v \\\hline 
(E)	& $m_{4,5,6,7}$ & 0 & 1 & \texttt{-} & \texttt{-} \\\hline \hline 
(F)	& $m_{6,7,14,15}$ & \texttt{-} & 1 & 1 & \texttt{-} \\\hline 
\end{tabular} \end{center}
\newpage \noindent Copertura minima: individuo le colonne essenziali (che coprono implicanti non coperti da altre)
\begin{center}\begin{tabular}{l|cccccc}
		         & A & B & C & D & \textbf{E} & \textbf{F} \\\hline 
		$m_{1}$  & 1 & 1 & 0 & 0 & 0 & 0 \\\hline \hline
		$m_{4}$  & 0 & 0 & 0 & 0 & \textbf{1} & 0 \\\hline \hline
		$m_{5}$  & 1 & 0 & 0 & 0 & 1 & 0 \\
		$m_{6}$  & 0 & 0 & 0 & 0 & 1 & 1 \\
		$m_{7}$  & 0 & 0 & 0 & 0 & 1 & 1 \\
		$m_{9}$  & 0 & 1 & 1 & 0 & 0 & 0 \\
		$m_{11}$ & 0 & 0 & 1 & 1 & 0 & 0 \\\hline \hline
		$m_{14}$ & 0 & 0 & 0 & 0 & 0 & \textbf{1} \\\hline \hline
		$m_{15}$ & 0 & 0 & 0 & 1 & 0 & 1 
	\end{tabular}
\end{center}
Elimino le colonne dominate (per le quali esiste un'altra colonna che ne copre tutti gli implicanti coperti) o le righe dominanti
\begin{center}\begin{tabular}{l|cccc}
		& A & B & C & D \\\hline 
		$m_{1}$  & {\color{gray}1} & \textbf{1} & 0 & 0 \\
		$m_{9}$  & 0 & 1 & 1 & 0 \\
		$m_{11}$ & 0 & 0 & \textbf{1} & {\color{gray}1} \\
	\end{tabular}
\end{center}
Quindi, il circuito semplificato è $$E+F+B+C = \overline{x}\ y+y\ z+\overline{y}\ \overline{z}\ v+x\ \overline{y}\ v$$ 
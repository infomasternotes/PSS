TEXC=pdflatex
TEXF=
TARGET=PSS
RM=rm -rf
OUTEXT=pdf
DEPFILES=pyplots
PYPLOTS=sigmadeltasignals dynamics

$(TARGET).pdf: $(TARGET).tex $(DEPFILES) 
	make build
	make clean
pyplots: ./py/*.py
	mkdir -p pyplots
	for f in $(PYPLOTS) ; do \
		./py/$$f.py; \
	done
	
.PHONY: compile build clean reset
compile:
	$(TEXC) $(TEXF) $(TARGET).tex
build:
	for i in 1 2 3 ; do \
		make compile ; \
	done
clean:
	if [ -f $(TARGET).tex ]; then \
		cp $(TARGET).tex ._temptex; \
	fi;
	if [ -f $(TARGET).$(OUTEXT) ]; then \
                cp $(TARGET).$(OUTEXT) ._temp$(OUTEXT); \
        fi;
	$(RM) $(TARGET).*
	if [ -f ._temptex ]; then \
		cp ._temptex $(TARGET).tex; \
	fi;
	if [ -f ._temp$(OUTEXT) ]; then \
		cp ._temp$(OUTEXT) $(TARGET).$(OUTEXT); \
	fi;
	$(RM) ._temp*

reset:
	$(RM) $(TARGET).$(OUTEXT)
	for f in $(DEPFILES); do \
		$(RM) $$f; \
	done
	make clean

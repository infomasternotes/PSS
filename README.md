# PSS

## Downloads
Se non ti interessa contribuire, ti sarà più comodo scaricare direttamente il file pdf degli appunti piuttosto che clonare il repository.  
[Download diretto del pdf dell'ultima release](https://gitlab.com/infomasternotes/PSS/-/jobs/artifacts/master/raw/PSS.pdf?job=pdf)  
[Download diretto del pdf dell'ultima beta](https://gitlab.com/infomasternotes/PSS/-/jobs/artifacts/develop/raw/PSS.pdf?job=pdf)

## Contribuire 
Se noti alcune cose che ti sembrano poco chiare o scorrette puoi apportare le tue modifiche e fare una *merge request*.

### Build
La build è automatizzata con **make**. Se utilizzi un editor LaTeX, prima dovrai eseguire *make*, che provvederà alla generazione di alcune figure.  
Vengono usati il compilatore **pdflatex** e i pacchetti della distribuzione **texlive**. Per quanto riguarda i plot python, vengono usate le librerie **matplotlib.pyplot** e **numpy** di **python3**.

#### Gitlab CI
Una volta caricato il repository su GitLab, il processo di build viene eseguito in un container Docker. 
Se non hai le dipendenze necessarie sulla tua macchina e non vuoi scaricarle per apportare piccole modifiche, puoi sfruttare questo servizio.

### File System
Nella directory principale sono presenti il file tex principale *PSS.tex*, il *makefile*, il file di configurazione per la CI *.gitlab-ci.yml*, questo *README.md* e il file *.gitignore*. 
Nella cartella *tex* sono presenti i file tex dei capitoli, nella cartella *img* sono presenti le immagini salvate, nella cartella *py* gli script python per i plot e nella cartella *pyplots* i plot generati.

### Figure
Se vuoi integrare con delle figure, per favore includile come mostrato sotto per coerenza rispetto a quelle già presenti  
`\begin{figure} \centering
	\caption{<caption>}
	\label{fig:<figlabel>}
	<image>
\end{figure}`  
Sono molto gradite immagini vettoriali (meglio se in .pdf o generate direttamente con codice LaTeX o python)

#### Pyplots
Per produrre figure in python3 con *pyplot*, aggiungi i relativi script nella cartella *py*. Come convenzione, tali script producono le figure sotto la cartella *pyplots* e non necessitano di parametri da riga di comando. 
Inoltre, deve poter essere eseguito *come script* e *matplotlib.pyplot* deve funzionare nel container Docker: per questo il codice python3 dovrebbe includere queste righe iniziali  
`#!/usr/bin/env python3
import matplotlib
matplotlib.use('Agg')`  
Rispettando queste convenzioni, la generazione dei plot è integrata nel processo di build con *make* inserendo nella variabile *PYPLOTS* del *makefile* il nome dello script generatore.

#!/usr/bin/env python3

import numpy
import plotutils

def main(sR=2**5,clkR=2**16,tDef=2**19,tMin=0,tMax=1,splots=[1,1,1,1,1,1,1,1],nX=7,nY=5,vMax=1,vMin=-1,obit=8,clrA='b',clrD='#32CD32'):
	# Reference signals
	T = numpy.arange(tMin,tMax,1/tDef)
	clk  = numpy.sin(2*numpy.pi*clkR*T) > 0
	clkS = numpy.sin(2*numpy.pi*sR*T)   > 0
	g = (vMax-vMin)/2
	m = (vMax+vMin)/2
	nDB = -300
	F = 4/3
	iFM = 0*F*2**(2)
	vIn = (g-10**(nDB/20)*2)*numpy.sin(2*numpy.pi*F*T + iFM*numpy.cumsum(numpy.random.normal(0,1,T.size))/tDef - numpy.pi/2)+m + 10**(nDB/20)*numpy.random.normal(0,1,T.size)
	# Preallocation
	tcurr = tMin-1/tDef
	icurr = 0
	acurr = vMin+(vMax-vMin)*(vIn[0]<m)
	D   = numpy.zeros(T.size)
	I   = numpy.zeros(T.size)
	C   = numpy.zeros(T.size)
	L   = numpy.zeros(T.size)
	A   = numpy.zeros(T.size)
	O   = numpy.zeros(T.size)
	BUF = numpy.zeros(int(numpy.floor(clkR/sR)))
	# Convertion
	for i in range(0,T.size):
		D[i] = vIn[i]-acurr
		I[i] = icurr + D[i]*(T[i]-tcurr)
		C[i] = I[i] > 0
		if (i == 0) or (clk[i]>clk[i-1]):
			L[i] = C[i]
			BUF[0:-1] = BUF[1:]
			BUF[-1] = L[i]
		else:
			L[i] = L[i-1]
		if (i==0) or (clkS[i]>clkS[i-1]):
			O[i] = min([ int(numpy.floor(sum(BUF)/len(BUF)*2**obit)) , 2**obit-1 ])
		else:			
			O[i] = O[i-1]
		A[i] = 2*(L[i]-0.5)*g+m
		tcurr = T[i]
		icurr = I[i]
		acurr = A[i]
	# Plots
	if splots[0]:
		plotutils.doSubplot(4,2,1,T,vIn,tMin,tMax,nX,False,vMin,vMax,nY,title='Analog Input',linecolor=clrA)
	if splots[1]:
		plotutils.doSubplot(4,2,2,T,C,tMin,tMax,nX,False,0,1,2,title='Comparator',linecolor=clrD)	
	if splots[2]:
		plotutils.doSubplot(4,2,3,T,D,tMin,tMax,nX,False,vMin-vMax,vMax-vMin,nY,title='Difference',linecolor=clrA)
	if splots[3]:
		plotutils.doSubplot(4,2,4,T,clk,tMin,tMax,nX,False,0,1,2,title='Latch Clock',linecolor=clrD)
	if splots[4]:
		plotutils.doSubplot(4,2,5,T,I,tMin,tMax,nX,False,plotutils.limits(I,0.1)[0],plotutils.limits(I,0.1)[1],nY,title='Integral',linecolor=clrA)
	if splots[5]:
		plotutils.doSubplot(4,2,6,T,L,tMin,tMax,nX,False,0,1,2,title='Latch',linecolor=clrD)
	if splots[6]:
		plotutils.doSubplot(4,2,7,T,O,tMin,tMax,nX,True,0,2**obit,nY,title='Digital Output',linecolor=clrD)
	if splots[7]:
		plotutils.doSubplot(4,2,8,T,A,tMin,tMax,nX,True,vMin,vMax,nY,title='Analog Feedback',linecolor=clrA)

if __name__ == "__main__":
	OUTNAME = 'pyplots/sigmadeltasignals.pdf'
	plotutils.layout()
	
	main(splots=[0,0,0,0,0,0,1,0],obit=5)
	main(splots=[1,1,1,1,1,1,0,1],clkR=2**6,tDef=2**9)
	
	plotutils.plot2pdf(OUTNAME)

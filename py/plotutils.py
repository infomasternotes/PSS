import numpy
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as pyplot
from matplotlib.backends.backend_pdf import PdfPages

def plot2pdf(fname):
	pdfout = PdfPages(fname)
	pdfout.savefig()
	pdfout.close()
def doTicks(n=0,a=0,b=1,y=False,labels=False,decimals=2):
	t = [a+(b-a)*i/(n-1) for i in range(n)]
	if labels:
		l = [ numpy.floor(i*(10**decimals))*(10**(-decimals)) for i in t]
	else:
		l = [' ' for i in t]
	if y:
		pyplot.yticks(t,l)
	else:
		pyplot.xticks(t,l)
def doSubplot(r,c,i,X,Y,xMin,xMax,nX,xLabels,yMin,yMax,nY,title=' ',linecolor='b',overlims=0.1,yLabels=True):
	ax = pyplot.subplot(r,c,i)
	pyplot.plot(X,Y,c=linecolor)
	doTicks(nX,xMin,xMax,labels=xLabels)
	decs = 1-int(numpy.floor(numpy.log10((yMax-yMin)/nY)))
	doTicks(nY,yMin,yMax,y=True,labels=yLabels,decimals=decs)
	pyplot.xlim([xMin,xMax])
	pyplot.ylim([yMin-(yMax-yMin)*overlims/2,yMax+(yMax-yMin)*overlims/2])
	pyplot.grid(visible=True,linestyle='-',alpha=0.2)
	ax.set_title(title)
def limits(A,x=1):
	aMin = min(A)
	aMax = max(A)
	d = aMax-aMin
	return [aMin-d*x,aMax+d*x]
def layout(aspect=[16,9],K=1,sub_h=3/4):
	pyplot.figure(figsize=(aspect[0]*K,aspect[1]*K))
	pyplot.subplots_adjust(hspace=sub_h)


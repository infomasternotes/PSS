#!/usr/bin/env python3

import numpy
import plotutils
import matplotlib.pyplot as pyplot

PALETTE = ['b','r','g']

def mylogistic(x,m=0,s=1,aMin=0,aMax=1):
	return aMin + (aMax-aMin)/(1+numpy.exp(-s*(x-m)))

def riseTime(t0r=0.1,tDef=2**10,tMin=0,tMax=1,splotI=1,nX=7,nY=11,yMax=1,yMin=0,colors=PALETTE,downsample=2**4):
	t0 = tMin+t0r*(tMax-tMin);
	
	k = 2**4
	
	T = numpy.arange(tMin,tMax,1/tDef)
	A = yMin + (yMax-yMin)*(T >= t0)
	for t in T:
		if mylogistic(-t,s=k) <= 0.10:
			t10 = -t
			break
	O = mylogistic(T,t0+6.28/k,k,yMin,yMax)
	for i in range(T.size):
		if O[i] > 0.10:
			off10 = i-1
			break
	for i in range(T.size-1,-1,-1):
		if O[i] < 0.90:
			off90 = i+1
			break
	# Plots
	plotutils.doSubplot(1,3,splotI,T[::downsample],A[::downsample],tMin,tMax,nX,False,yMin,yMax,nY,yLabels='False',linecolor=colors[0])
	pyplot.hold(True)
	pyplot.plot(T[::downsample],O[::downsample],c=colors[1])
	pyplot.plot([T[off10],T[off10]],[yMin,O[off10]],c=colors[2],linestyle='--')
	pyplot.plot([T[off90],T[off90]],[yMin,O[off90]],c=colors[2],linestyle='--')
	pyplot.plot([T[off10],T[off90]],[yMin,yMin],c=colors[2],linewidth=3)
	pyplot.text((T[off10]+T[off90])/2,yMin-(yMax-yMin)*0.0075,'Rise Time',verticalalignment='top',horizontalalignment='center')

def timeConst(t0r=0.1,tDef=2**10,tMin=0,tMax=1,splotI=3,nX=7,nY=11,yMax=1,yMin=0,colors=PALETTE,downsample=2**3):
	t0 = tMin+t0r*(tMax-tMin);
	
	TK = 0.2
	
	T = numpy.arange(tMin,tMax,1/tDef)
	A = yMin + (yMax-yMin)*(T >= t0)
	O = 1-numpy.exp(-(T-t0)/TK)
	O = O*(O>0)*(yMax-yMin)+yMin
	#Plots
	plotutils.doSubplot(1,3,splotI,T[::downsample],A[::downsample],tMin,tMax,nX,False,yMin,yMax,nY,yLabels='False',linecolor=colors[0])
	pyplot.hold(True)
	pyplot.plot(T[::downsample],O[::downsample],c=colors[1])
	pyplot.plot([t0,t0+TK],[yMin,yMax],c=colors[2],linestyle='--')
	pyplot.plot([t0,t0+TK],[yMin,yMin],c=colors[2],linewidth=3)
	pyplot.plot([t0+TK,t0+TK],[yMin,yMax],c=colors[2],linestyle='--')
	pyplot.text(t0+TK/2,yMin-(yMax-yMin)*0.0075,'Time Constant',verticalalignment='top',horizontalalignment='center')

def slewRate(t0r=2**(-2),tDef=2**10,tMin=0,tMax=1,splotI=2,nX=7,nY=11,yMax=1,yMin=0,colors=PALETTE,downsample=2**3):
	t0 = tMin+t0r*(tMax-tMin);
	
	F = 7/3
	S = (yMax-yMin)*F*7/5
	
	T = numpy.arange(tMin,tMax,1/tDef)
	A = (yMin+yMax)/2 + (yMax-yMin)/2*numpy.sin(2*numpy.pi*F*T)*(1-numpy.exp(-T/t0))
	O = numpy.zeros(T.size)
	
	dS = S/tDef
	O[0] = A[0]
	for i in range(1,T.size):
		if numpy.absolute(A[i]-O[i-1]) > dS:
			O[i] = O[i-1]+numpy.sign(A[i]-O[i-1])*dS
		else:
			O[i] = A[i]
	#Plots
	plotutils.doSubplot(1,3,splotI,T[::downsample],A[::downsample],tMin,tMax,nX,False,yMin,yMax,nY,yLabels='False',linecolor=colors[0])
	pyplot.hold(True)
	pyplot.plot(T[::downsample],O[::downsample],c=colors[1])
	tX = [4.3/4/F , 5/4/F]
	tI = [0,0]
	for i in range(T.size):
		if T[i] <= tX[0]:
			tI = [i,tI[1]]
		if T[i] <= tX[1]:
			tI = [tI[0],i]
	
	pyplot.plot([T[tI[0]],T[tI[0]]],[yMin,O[tI[0]]],c=colors[2],linestyle='--')
	pyplot.plot([T[tI[1]],T[tI[1]]],[yMin,O[tI[1]]],c=colors[2],linestyle='--')
	pyplot.plot([T[tI[0]],T[tI[1]]],[O[tI[0]],O[tI[1]]],c=colors[2],linewidth=3)
	pyplot.text(T[tI[0]],(O[tI[0]]+O[tI[1]])/2,'Slew Rate  ',horizontalalignment='right',verticalalignment='center')

if __name__ == "__main__":
	OUTNAME = 'pyplots/dynamics.pdf'
	plotutils.layout([4,1],5)
	
	riseTime()
	slewRate()
	timeConst()
	
	plotutils.plot2pdf(OUTNAME)
